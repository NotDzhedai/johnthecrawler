package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/tinne26/etxt"
)

type CombatText struct {
	*Rect
	text           string
	color          color.Color
	fontSize       int
	canDelete      bool
	timer          float64
	ascentSpeed    float64
	ascentDuration float64
}

func NewCombatText(x, y float64, text string, duration float64, fontSize int, color color.Color) *CombatText {
	ct := &CombatText{
		Rect:           &Rect{x: x, y: y},
		text:           text,
		color:          color,
		fontSize:       fontSize,
		canDelete:      false,
		timer:          0,
		ascentDuration: duration,
		ascentSpeed:    1,
	}

	return ct
}

func (ct *CombatText) Update(cameraX, cameraY float64) {
	ct.x += cameraX
	ct.y += cameraY

	ct.timer++
	ct.y -= ct.ascentSpeed
	if ct.timer >= ct.ascentDuration*FPS {
		ct.canDelete = true
	}
}

func (ct *CombatText) Draw(screen *ebiten.Image, textRenderer *etxt.Renderer) {
	textRenderer.SetTarget(screen)

	textRenderer.SetColor(ct.color)
	textRenderer.SetSizePx(ct.fontSize)

	textRenderer.Draw(ct.text, int(ct.x), int(ct.y))
}
