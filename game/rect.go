package game

import "math"

type Line struct {
	X1, Y1, X2, Y2 float64
}

func (l *Line) Angle() float64 {
	return math.Atan2(l.Y2-l.Y1, l.X2-l.X1)
}

type Rect struct {
	x, y          float64
	width, height float64
}

func (r *Rect) ToLines() []Line {
	return []Line{
		{r.x, r.y, r.x, r.y + r.height},
		{r.x, r.y + r.height, r.x + r.width, r.y + r.height},
		{r.x + r.width, r.y + r.height, r.x + r.width, r.y},
		{r.x + r.width, r.y, r.x, r.y},
	}
}

func (r *Rect) CenterXY() (float64, float64) {
	cx := (r.x + r.width/2)
	cy := (r.y + r.height/2)
	return cx, cy
}

func (r *Rect) CenterX() float64 {
	return r.x + r.width/2
}

func (r *Rect) CenterY() float64 {
	return r.y + r.height/2
}

func (r *Rect) XY() (float64, float64) {
	return r.x, r.y
}

func (r *Rect) OutOfWindow(w, h float64) bool {
	return r.x < 0 || r.x >= w ||
		r.y < 0 || r.y >= h
}

func (r *Rect) IsCollide(target *Rect) bool {
	return r.x < target.x+target.width &&
		r.x+r.width > target.x &&
		r.y < target.y+target.height &&
		r.y+r.height > target.y
}
