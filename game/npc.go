package game

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Npc interface {
	Update(cameraX, cameraY float64, g *Game)
	Draw(screen *ebiten.Image)

	GetDialog() *Dialog
	GetIcon() *ebiten.Image
	GetName() string
	NextDialog(int)
	IsAlive() bool
	GetInventory() *Inventory
	GetStats() *CharacterStats
}

type BaseNpc struct {
	name string
	*CharacterAnimator
	*Mover
	*CharacterStats

	greetingSound []byte
	isVisible     bool

	dialogRange float64
	dialog      *Dialog
	inventory   *Inventory
}

func NewBaseNpc(name string, x, y float64, sprites [][]*ebiten.Image, sound []byte, d *Dialog) *BaseNpc {
	npc := &BaseNpc{
		name:              name,
		CharacterAnimator: NewCharacterAnimator(x, y, sprites),
		CharacterStats: NewStats(&CharacterStatsConfig{
			Health: 1000,
			Mana:   1000,
		}),
		dialogRange:   1 * TileSize,
		dialog:        d,
		inventory:     NewInventory(),
		greetingSound: sound,
	}
	npc.isVisible = true

	return npc
}

func (npc *BaseNpc) GetDialog() *Dialog {
	return npc.dialog
}

func (npc *BaseNpc) GetIcon() *ebiten.Image {
	return npc.sprites[0][0]
}

func (npc *BaseNpc) GetName() string {
	return npc.name
}

func (npc *BaseNpc) NextDialog(idx int) {
	npc.dialog = npc.dialog.Choises[idx].Next
}

func (npc *BaseNpc) GetInventory() *Inventory {
	return npc.inventory
}

func (npc *BaseNpc) GetStats() *CharacterStats {
	return npc.CharacterStats
}

func (npc *BaseNpc) inDialogRange(sx, sy, tx, ty float64) bool {
	return Distance(sx, sy, tx, ty) <= npc.dialogRange
}

func (npc *BaseNpc) Update(cameraX, cameraY float64, g *Game) {
	sx, sy := npc.CenterXY()
	px, py := g.player.CenterXY()

	if inpututil.IsKeyJustPressed(ebiten.KeyT) && npc.inDialogRange(sx, sy, px, py) {
		g.audioPlayer.PlaySound(npc.greetingSound)
		g.sm.Change(DialogStateType, npc)
	}

	npc.CharacterAnimator.Update(cameraX, cameraY, g.currentLevel)
}

func (npc *BaseNpc) Draw(screen *ebiten.Image) {
	if npc.isVisible {
		npc.CharacterAnimator.Draw(screen, GAP)
	}
}

// TODO randomize
func addItemsToSell(inv *Inventory, a *Assets) {
	inv.Add(NewRedPotion(0, 0, 10, a.PotionSprites[Red]))
	inv.Add(NewBluePotion(0, 0, 10, a.PotionSprites[Blue]))
	inv.Add(NewBluePotion(0, 0, 10, a.PotionSprites[Blue]))
	inv.Add(NewBluePotion(0, 0, 10, a.PotionSprites[Blue]))
	inv.Add(NewBluePotion(0, 0, 10, a.PotionSprites[Blue]))
	inv.Add(NewBluePotion(0, 0, 10, a.PotionSprites[Blue]))
	inv.Add(NewRedPotion(0, 0, 10, a.PotionSprites[Red]))
	inv.Add(NewBluePotion(0, 0, 10, a.PotionSprites[Blue]))
	inv.Add(NewRedPotion(0, 0, 10, a.PotionSprites[Red]))
	inv.Add(NewRedPotion(0, 0, 10, a.PotionSprites[Red]))
	inv.Add(NewRedPotion(0, 0, 10, a.PotionSprites[Red]))
}

// Npcs
func Isolda(x, y float64, a *Assets) *BaseNpc {
	firstDialog := &Dialog{Text: `Hello handsome, what do you need?`}

	response1 := &Dialog{Text: TradeDialog}
	response2 := &Dialog{Text: "Of course, I am the most beautiful in my clan!"}
	response3 := &Dialog{Text: EndDialog}

	firstDialog.AddChoice("What do you have for sale?", response1)
	firstDialog.AddChoice("You look... great!", response2)
	firstDialog.AddChoice("= End =", response3)

	response1.AddChoice("Back", firstDialog)
	response2.AddChoice("Back", firstDialog)

	i := NewBaseNpc(
		"Isolda",
		x,
		y,
		a.CharacterSprites[DwarfF],
		a.FOhMyGoodnessGracious,
		firstDialog,
	)
	i.inventory.gold = 1000
	addItemsToSell(i.inventory, a)

	return i
}

func Robert(x, y float64, a *Assets) *BaseNpc {
	firstDialog := &Dialog{Text: `Hello, what do you need?`}

	response1 := &Dialog{Text: TradeDialog}
	response2 := &Dialog{Text: "Earn my respect and then we'll talk"}
	response3 := &Dialog{Text: EndDialog}

	firstDialog.AddChoice("I want to trade with you", response1)
	firstDialog.AddChoice("Give me discounts on your products", response2)
	firstDialog.AddChoice("= End =", response3)

	response1.AddChoice("Back", firstDialog)
	response2.AddChoice("Back", firstDialog)

	i := NewBaseNpc(
		"Robert",
		x,
		y,
		a.CharacterSprites[DwarfM],
		a.MWhatCanIDoForYouSound,
		firstDialog,
	)
	i.inventory.gold = 1000
	addItemsToSell(i.inventory, a)

	return i
}

func Ksenia(x, y float64, a *Assets) *BaseNpc {
	firstDialog := &Dialog{
		Text: `Welcome! Who are you? What are you doing here? 
Nowadays it is quite difficult to find people brave enough for 
these caves`,
	}
	response1 := &Dialog{Text: "Interesting cause, how do you say your name?"}
	response2 := &Dialog{Text: "As you like! I hope the monsters kill you"}
	response3 := &Dialog{Text: "Nope, fuck you!"}
	response4 := &Dialog{Text: EndDialog}

	firstDialog.AddChoice("Hi! I decided to die covered with glory", response1)
	firstDialog.AddChoice("It's none of your business!", response2)
	firstDialog.AddChoice("Fuck you!", response3)
	firstDialog.AddChoice("= End =", response4)

	response1.AddChoice("Back", firstDialog)
	response2.AddChoice("Back", firstDialog)
	response3.AddChoice("Back", firstDialog)
	response4.AddChoice("Back", firstDialog)

	response1_1 := &Dialog{Text: "I doubt it"}
	response1_2 := &Dialog{Text: "I will call you Mr. Idiot"}

	response1.AddChoice("My name is John the Crawler! And I will become a legend!", response1_1)
	response1.AddChoice("You are not worthy to know my name!", response1_2)

	response1_1.AddChoice("Back", response1)
	response1_2.AddChoice("Back", response1)

	i := NewBaseNpc(
		"Ksenia",
		x,
		y,
		a.CharacterSprites[LizardF],
		a.FOkey,
		firstDialog,
	)

	return i
}

func Arnold(x, y float64, a *Assets) *BaseNpc {
	firstDialog := &Dialog{
		Text: `Ohh, it's nice to see young talents.
I used to be like you, but I got shot in the knee.`,
	}
	response1 := &Dialog{Text: "My age..."}
	response2 := &Dialog{Text: "Strong enough to kill you"}
	response3 := &Dialog{Text: EndDialog}

	firstDialog.AddChoice("What happened?", response1)
	firstDialog.AddChoice("A weak and pathetic dinosaur!", response2)
	firstDialog.AddChoice("= End =", response3)

	response1.AddChoice("Back", firstDialog)
	response2.AddChoice("Back", firstDialog)
	response3.AddChoice("Back", firstDialog)

	i := NewBaseNpc(
		"Arnold",
		x,
		y,
		a.CharacterSprites[LizardM],
		a.MStayAwhileAndListerSound,
		firstDialog,
	)

	return i
}
