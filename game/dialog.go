package game

const TradeDialog = "[TRADE]"
const EndDialog = "[END]"

type Choice struct {
	Text string
	Next *Dialog
}

type Dialog struct {
	Text    string
	Choises []*Choice
}

func (d *Dialog) IsNextTrade(idx int) bool {
	return d.Choises[idx].Next.Text == TradeDialog
}

func (d *Dialog) IsNextEnd(idx int) bool {
	return d.Choises[idx].Next.Text == EndDialog
}

func (d *Dialog) AddChoice(text string, next *Dialog) {
	c := &Choice{Text: text, Next: next}
	d.Choises = append(d.Choises, c)
}
