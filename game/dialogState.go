package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/tinne26/etxt"
)

const (
	DialogOptionsStartX = ScreenWidth / 2
	DialogOptionsStartY = 625
)

type dialogState struct {
	npc Npc

	rect Rect

	dialogWindow *ebiten.Image
	npcWindow    *ebiten.Image
	op           *ebiten.DrawImageOptions
}

func newDialogState(rect Rect) *dialogState {
	ds := &dialogState{
		rect:         rect,
		op:           &ebiten.DrawImageOptions{},
		dialogWindow: ebiten.NewImage(int(rect.width), int(rect.height)),
		npcWindow:    ebiten.NewImage(120, 145),
	}

	ds.npcWindow.Fill(color.RGBA{181, 179, 168, 255})

	return ds
}

func (ds *dialogState) update(g *Game) {
	if inpututil.IsKeyJustPressed(ebiten.KeyT) || inpututil.IsKeyJustPressed(ebiten.KeyEscape) {
		g.sm.Change(MainStateType)
	}

	cx, cy := ebiten.CursorPosition()
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
		idx := ds.getOptionIdx(cx, cy)
		if idx != -1 {
			if ds.npc.GetDialog().IsNextTrade(idx) {
				g.sm.Change(TradeStateType, ds.npc, g)
			} else if ds.npc.GetDialog().IsNextEnd(idx) {
				g.sm.Change(MainStateType)
			} else {
				ds.npc.NextDialog(idx)
			}
		}
	}
}

func (ds *dialogState) getOptionIdx(cx, cy int) int {
	if cx < int(ds.rect.x) || cx > int(ds.rect.x+ds.rect.width) ||
		cy < DialogOptionsStartY || cy > DialogOptionsStartY+(30*len(ds.npc.GetDialog().Choises)) {
		return -1
	}

	idx := (cy - DialogOptionsStartY) / 30
	if idx >= len(ds.npc.GetDialog().Choises) {
		return -1
	}

	return idx
}

func (ds *dialogState) draw(screen *ebiten.Image, g *Game) {
	// draw main state
	g.sm.states[MainStateType].draw(screen, g)

	// clear dialog window
	ds.dialogWindow.Fill(UIColor)

	// dialog window
	ds.op.GeoM.Reset()
	ds.op.GeoM.Translate(ds.rect.XY())
	screen.DrawImage(ds.dialogWindow, ds.op)

	// npc avatar background image
	ds.op.GeoM.Reset()
	ds.op.GeoM.Translate(ds.rect.x+35, ds.rect.y-90)
	screen.DrawImage(ds.npcWindow, ds.op)

	// npc avatar image
	ds.op.GeoM.Reset()
	ds.op.GeoM.Scale(5, 5)
	ds.op.GeoM.Scale(1, 1)
	ds.op.GeoM.Translate(ds.rect.x+50, ds.rect.y-100)
	screen.DrawImage(ds.npc.GetIcon(), ds.op)

	// setup text renderer
	g.textRenderer.SetTarget(screen)
	g.textRenderer.SetColor(color.Black)
	g.textRenderer.SetSizePx(30)
	// npc name
	g.textRenderer.Draw(ds.npc.GetName(), int(ds.rect.x+210), int(ds.rect.y+25))

	// DIALOG
	// dialog text
	g.textRenderer.SetSizePx(25)
	g.textRenderer.SetAlign(etxt.Top, etxt.Left)
	g.textRenderer.Draw(ds.npc.GetDialog().Text, int(ds.rect.x+50), int(ds.rect.y+80))
	// dialog options
	g.textRenderer.SetSizePx(20)
	g.textRenderer.SetAlign(etxt.Top, etxt.XCenter)

	cx, cy := ebiten.CursorPosition()
	idx := ds.getOptionIdx(cx, cy)

	for i, o := range ds.npc.GetDialog().Choises {
		if idx == i {
			g.textRenderer.SetColor(color.RGBA{255, 0, 0, 255})
		}
		g.textRenderer.Draw(o.Text, DialogOptionsStartX, DialogOptionsStartY+(i*30))
		g.textRenderer.SetColor(color.Black)
	}
}

func (ds *dialogState) enter(args ...any) {
	if len(args) != 1 {
		panic("wrong number of args in dialog state enter method")
	}

	npc, ok := args[0].(Npc)
	if !ok {
		panic("wrong 1 arg in enter dialog state")
	}
	ds.npc = npc
}

func (ds *dialogState) exit() {
	ds.npc = nil
}
