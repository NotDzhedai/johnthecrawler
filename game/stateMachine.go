package game

import "github.com/hajimehoshi/ebiten/v2"

type stateType int

const (
	MainStateType stateType = iota
	InventoryStateType
	DialogStateType
	TradeStateType
	StartGameStateType
)

type state interface {
	enter(...any)
	exit()
	update(g *Game)
	draw(screen *ebiten.Image, g *Game)
}

type StateMachine struct {
	states  map[stateType]state
	current state
}

func NewStateMachine(inventoryRect, dialogRect, tradeRect Rect, a *Assets) *StateMachine {
	initState := NewStartGameState(a)
	return &StateMachine{
		states: map[stateType]state{
			MainStateType:      newMainState(),
			InventoryStateType: newInventoryState(inventoryRect),
			DialogStateType:    newDialogState(dialogRect),
			TradeStateType:     newTradeState(tradeRect),
			StartGameStateType: initState,
		},
		current: initState,
	}
}

func (sm *StateMachine) Update(g *Game) {
	sm.current.update(g)
}

func (sm *StateMachine) Draw(screen *ebiten.Image, g *Game) {
	sm.current.draw(screen, g)
}

func (sm *StateMachine) Change(st stateType, args ...any) {
	if sm.current != nil {
		sm.current.exit()
	}
	sm.current = sm.states[st]
	sm.current.enter(args...)
}
