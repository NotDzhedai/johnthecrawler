package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type InventoryMode int

const (
	InventoryToUse InventoryMode = iota
	InventoryToTrade
)

type inventoryState struct {
	rect Rect

	inventoryWindow *ebiten.Image
	op              *ebiten.DrawImageOptions
	mode            InventoryMode
	vendorInv       *Inventory
	inventoryStartX float64
	inventoryStartY float64
}

func newInventoryState(rect Rect) *inventoryState {
	return &inventoryState{
		rect:            rect,
		op:              &ebiten.DrawImageOptions{},
		inventoryWindow: ebiten.NewImage(int(rect.width), int(rect.height)),
		mode:            InventoryToUse,
		inventoryStartX: rect.x + 10 - InventoryIconSize,
		inventoryStartY: 300,
	}
}

func (i *inventoryState) update(g *Game) {
	// close inventory
	if inpututil.IsKeyJustPressed(ebiten.KeyI) || inpututil.IsKeyJustPressed(ebiten.KeyEscape) {
		g.sm.Change(MainStateType)
	}

	// update some players fields to use it from inventory state
	g.player.CheckPotionCooldown()
	g.player.HandleWeaponChange()

	// handle inventory clicks
	cx, cy := ebiten.CursorPosition()
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
		slot, idx := GetInventorySlot(
			float64(cx), float64(cy),
			i.inventoryStartX, i.inventoryStartY,
			g.player.inventory,
		)

		if slot != nil {
			switch i.mode {
			case InventoryToUse:
				if slot.Use(g.player) {
					g.player.inventory.Delete(idx)
				}
			case InventoryToTrade:
				if i.vendorInv.gold >= slot.Price() {
					i.vendorInv.Add(slot)
					i.vendorInv.gold -= slot.Price()
					g.player.inventory.Delete(idx)
					g.player.inventory.gold += slot.Price()
				}
			}
		}
	}
}

func (i *inventoryState) draw(screen *ebiten.Image, g *Game) {
	// show paused main state when inventory is opened
	g.sm.states[MainStateType].draw(screen, g)

	// draw inventory window
	// clear prev inventory window
	i.inventoryWindow.Fill(UIColor)

	// setup text renderer
	g.textRenderer.SetTarget(i.inventoryWindow)
	g.textRenderer.SetColor(color.Black)

	// player info
	DrawCharacterInfo(
		g.textRenderer,
		i.op,
		i.inventoryWindow,
		g.player.sprites[0][0],
		"John the Crawler", 1,
		g.player.CharacterStats,
		g.player.inventory,
	)

	// Weapons
	i.drawPlayerWeapons(g)

	// inventory header
	DrawInventoryHeader(g.textRenderer, i.op, i.inventoryWindow, screen, i.rect, g.player.inventory)

	// I'm drawing on the main screen to maintain global coordinates.
	// This is because ebiten.CursorPosition() provides coordinates within the game window.
	// I haven't found another way yet.
	DrawInventory(
		i.op,
		screen,
		g.player.inventory,
		i.inventoryStartX, i.inventoryStartY,
	)

}

func (i *inventoryState) drawPlayerWeapons(g *Game) {
	g.textRenderer.Draw("Weapons: ", 150, 145)
	for idx, w := range g.player.weapons {
		i.op.GeoM.Reset()
		if idx == g.player.activeWeapon {
			vector.DrawFilledRect(
				i.inventoryWindow,
				225+float32(idx*30),
				153,
				float32(w.GetIcon().Bounds().Dx()+10),
				5,
				color.RGBA{128, 255, 0, 255},
				true,
			)
		}
		i.op.GeoM.Translate(230+float64(idx*30), 120)
		i.inventoryWindow.DrawImage(w.GetIcon(), i.op)
	}
}

func (is *inventoryState) enter(args ...any) {
	if len(args) == 0 {
		is.mode = InventoryToUse
		return
	}

	// for trade state
	mode, ok := args[0].(InventoryMode)
	if !ok {
		panic("wrong 1 arg type in inventory enter method")
	}
	is.mode = mode

	vendorInv, ok := args[1].(*Inventory)
	if !ok {
		panic("wrong 2 arg type in inventory enter method")
	}
	is.vendorInv = vendorInv
}

func (is *inventoryState) exit() {
	is.vendorInv = nil
}
