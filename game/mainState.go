package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"golang.org/x/exp/slices"
)

type mainState struct{}

func newMainState() *mainState {
	return &mainState{}
}

func temp(p *Player) {
	// ! temp
	if inpututil.IsKeyJustPressed(ebiten.KeyH) {
		p.health -= 10
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyM) {
		p.mana -= 10
	}
}

func (ms *mainState) update(g *Game) {
	// open inventory
	if inpututil.IsKeyJustPressed(ebiten.KeyI) {
		g.sm.Change(InventoryStateType)
	}

	// ! TODO delete later
	temp(g.player)

	// player
	cameraX, cameraY := g.player.Update(g)

	// level
	g.currentLevel.Update(cameraX, cameraY, g)

	// arrows
	for _, a := range g.arrows {
		ct := a.Update(cameraX, cameraY, g.currentLevel, g.audioPlayer)
		if ct != nil {
			g.combatTexts = append(g.combatTexts, ct)
		}
	}

	// combat text
	for _, ct := range g.combatTexts {
		ct.Update(cameraX, cameraY)
	}

	ms.clear(g)
}

func (ms *mainState) clear(g *Game) {
	g.combatTexts = slices.DeleteFunc(g.combatTexts, func(ct *CombatText) bool {
		return ct.canDelete
	})
	g.arrows = slices.DeleteFunc(g.arrows, func(a Arrow) bool {
		return a.CanDelete()
	})
}

func (ms *mainState) draw(screen *ebiten.Image, g *Game) {
	screen.Fill(color.Black)

	// level
	g.currentLevel.Draw(screen, g.player, g.debug)
	if g.debug.Camera {
		vector.DrawFilledRect(
			screen,
			CameraTreshholdX,
			CameraTreshholdY,
			ScreenWidth-(CameraTreshholdX*2),
			ScreenHeight-(CameraTreshholdY*2),
			color.RGBA{100, 100, 100, 100},
			false,
		)
	}

	// player
	g.player.Draw(screen, g)

	// arrows
	for _, a := range g.arrows {
		a.Draw(screen)
	}

	// combatTexts
	for _, ct := range g.combatTexts {
		ct.Draw(screen, g.textRenderer)
	}

	// game info
	DrawGameInfo(screen, g)

}

func (ms *mainState) enter(args ...any) {}
func (ms *mainState) exit()             {}
