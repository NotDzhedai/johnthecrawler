package game

import (
	"math"
	"sort"

	"github.com/hajimehoshi/ebiten/v2"
)

// sightRange - max range where player see
func (l *Level) rayCasting(cx, cy, sightRange, step float64) []Line {
	var rays []Line
	lines := make([]Line, 0)
	for _, obs := range l.obstacleTiles {
		if Distance(cx, cy, obs.x, obs.y) > 500 {
			continue
		}
		obsLines := obs.ToLines()

		// only inner walls
		if obsLines[2].X1 < cx && obsLines[2].X1 > obsLines[0].X1 {
			lines = append(lines, obsLines[2])
		}
		if obsLines[3].Y1 > cy && obsLines[3].Y1 < obsLines[1].Y1 {
			lines = append(lines, obsLines[3])
		}
		if obsLines[1].Y1 < cy && obsLines[1].Y1 > obsLines[3].Y1 {
			lines = append(lines, obsLines[1])
		}
		if obsLines[0].X1 > cy && obsLines[0].X1 < obsLines[2].X1 {
			lines = append(lines, obsLines[0])
		}

	}

	for i := 0.0; i <= 2*math.Pi; i += step {
		angle := i

		points := [][2]float64{}
		ray := newRay(cx, cy, sightRange, angle)

		// Unpack all objects
		for _, wall := range lines {
			if px, py, ok := intersection(ray, wall); ok {
				points = append(points, [2]float64{px, py})
			} else {
				points = append(points, [2]float64{ray.X2, ray.Y2})
			}
		}

		// Find the point closest to start of ray
		min := math.Inf(1)
		minI := -1
		for i, p := range points {
			d2 := (cx-p[0])*(cx-p[0]) + (cy-p[1])*(cy-p[1])
			if d2 < min {
				min = d2
				minI = i
			}
		}
		rays = append(rays, Line{cx, cy, points[minI][0], points[minI][1]})
	}

	// Sort rays based on angle, otherwise light triangles will not come out right
	sort.Slice(rays, func(i int, j int) bool {
		return rays[i].Angle() < rays[j].Angle()
	})
	return rays
}

func newRay(x, y, length, angle float64) Line {
	return Line{
		X1: x,
		Y1: y,
		X2: x + length*math.Cos(angle),
		Y2: y + length*math.Sin(angle),
	}
}

func intersection(l1, l2 Line) (float64, float64, bool) {
	denom := (l1.X1-l1.X2)*(l2.Y1-l2.Y2) - (l1.Y1-l1.Y2)*(l2.X1-l2.X2)
	tNum := (l1.X1-l2.X1)*(l2.Y1-l2.Y2) - (l1.Y1-l2.Y1)*(l2.X1-l2.X2)
	uNum := -((l1.X1-l1.X2)*(l1.Y1-l2.Y1) - (l1.Y1-l1.Y2)*(l1.X1-l2.X1))

	if denom == 0 {
		return 0, 0, false
	}

	t := tNum / denom
	if t > 1 || t < 0 {
		return 0, 0, false
	}

	u := uNum / denom
	if u > 1 || u < 0 {
		return 0, 0, false
	}

	x := l1.X1 + t*(l1.X2-l1.X1)
	y := l1.Y1 + t*(l1.Y2-l1.Y1)
	return x, y, true
}

func rayVertices(x1, y1, x2, y2, x3, y3 float64) []ebiten.Vertex {
	return []ebiten.Vertex{
		{DstX: float32(x1), DstY: float32(y1), SrcX: 0, SrcY: 0, ColorR: 1, ColorG: 1, ColorB: 1, ColorA: 1},
		{DstX: float32(x2), DstY: float32(y2), SrcX: 0, SrcY: 0, ColorR: 1, ColorG: 1, ColorB: 1, ColorA: 1},
		{DstX: float32(x3), DstY: float32(y3), SrcX: 0, SrcY: 0, ColorR: 1, ColorG: 1, ColorB: 1, ColorA: 1},
	}
}
