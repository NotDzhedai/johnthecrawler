package game

import (
	"github.com/hajimehoshi/ebiten/v2"
)

// animation states
const (
	IdleAnimation = iota
	RunningAnimation
)

type CharacterAnimator struct {
	*Animator
	*Rect
	flip bool

	op            *ebiten.DrawImageOptions
	sprites       [][]*ebiten.Image
	animationType int // 0 - idle, 1 - run

	isRunning bool
}

func NewCharacterAnimator(x, y float64, sprites [][]*ebiten.Image) *CharacterAnimator {
	e := &CharacterAnimator{
		Animator: NewAnimator(
			0.3,
			len(sprites[0]),
		),
		Rect: &Rect{
			x,
			y,
			float64(sprites[0][0].Bounds().Dx()) * PlayerScale,
			float64(sprites[0][0].Bounds().Dy()) * PlayerScale,
		},
		flip: false,

		op:            &ebiten.DrawImageOptions{},
		sprites:       sprites,
		animationType: IdleAnimation,
		isRunning:     false,
	}

	return e
}

// player Update
func (e *CharacterAnimator) Update(cameraX, cameraY float64, l *Level) {
	e.x += cameraX
	e.y += cameraY

	e.Animator.UpdateAnimIdx()

	if e.isRunning {
		e.updateAnimationType(RunningAnimation)
	} else {
		e.updateAnimationType(IdleAnimation)
	}
}

func (e *CharacterAnimator) updateAnimationType(new int) {
	if new != e.animationType {
		e.animationType = new
		e.animationIdx = 0
		e.animationTimer = 0
	}
}

func (e *CharacterAnimator) Draw(screen *ebiten.Image, yGap float64) {
	e.op.GeoM.Reset()

	e.op.GeoM.Scale(PlayerScale, PlayerScale)

	if e.flip {
		e.op.GeoM.Scale(-1, 1)
		e.op.GeoM.Translate(e.width, 0)
	} else if !e.flip {
		e.op.GeoM.Scale(1, 1)
	}

	e.op.GeoM.Translate(e.x, e.y-yGap)

	screen.DrawImage(e.sprites[e.animationType][e.animationIdx], e.op)
}
