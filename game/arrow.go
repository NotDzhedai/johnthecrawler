package game

import (
	"fmt"
	"image/color"
	"log"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
)

type Arrow interface {
	Update(cameraX, cameraY float64, l *Level, audioPlayer *AudioPlayer) *CombatText
	Draw(screen *ebiten.Image)
	MakeDamage(enemy *Enemy) *CombatText
	CanDelete() bool
}

type BaseArrow struct {
	*Rect
	*Animator
	*DamageStats

	sprites    []*ebiten.Image
	op         *ebiten.DrawImageOptions
	isAnimated bool
	isVisible  bool

	hitSound []byte

	angle    float64
	speed    float64
	scale    float64
	rotation float64

	flip bool

	offsetX, offsetY float64

	// for updating arrow
	dx, dy float64

	// for clearing arrow
	canDelete bool
}

func NewBaseArrow(
	images []*ebiten.Image,
	sound []byte,
	x float64,
	y float64,
	angle float64,
	scale float64,
	rotation float64,
	speed float64,
	ds *DamageStats,
	isAnimated bool,
) *BaseArrow {
	a := &BaseArrow{
		Animator: NewAnimator(
			0.2,
			len(images),
		),
		Rect: &Rect{
			x:      x,
			y:      y,
			width:  float64(images[0].Bounds().Dx()) * scale,
			height: float64(images[0].Bounds().Dy()) * scale,
		},
		hitSound:   sound,
		sprites:    images,
		op:         &ebiten.DrawImageOptions{},
		isAnimated: isAnimated,
		isVisible:  true,

		angle:    angle,
		speed:    speed,
		scale:    scale,
		rotation: rotation,
		flip:     false,

		DamageStats: ds,

		canDelete: false,
	}

	a.dx = math.Cos(angle) * a.speed
	a.dy = math.Sin(angle) * a.speed

	return a
}

func (a *BaseArrow) Update(cameraX, cameraY float64) *CombatText {
	a.x += a.dx + cameraX
	a.y += a.dy + cameraY

	if math.Abs(a.angle) > math.Pi/2 {
		a.flip = true
	} else if math.Abs(a.angle) < math.Pi/2 {
		a.flip = false
	}

	if a.isAnimated {
		a.Animator.UpdateAnimIdx()
	}

	if a.OutOfWindow(ScreenWidth, ScreenHeight) {
		a.canDelete = true
	}

	return nil
}

func (a *BaseArrow) MakeDamage(enemy *Enemy) *CombatText {
	damage := a.DealDamage(enemy.CharacterStats)
	log.Printf("Player attacked %s for %d\n", enemy.name, damage)

	return NewCombatText(
		enemy.CenterX(),
		enemy.y-10,
		fmt.Sprint(damage),
		0.5,
		25,
		color.RGBA{255, 0, 0, 255},
	)
}

func (a *BaseArrow) Draw(screen *ebiten.Image) {
	a.op.GeoM.Reset()

	if a.rotation != 0 {
		a.op.GeoM.Rotate(a.rotation)
		a.op.GeoM.Translate(a.width, 0)
	}
	if a.flip {
		a.op.GeoM.Scale(1, -1)
	} else if !a.flip {
		a.op.GeoM.Scale(1, 1)
	}

	a.op.GeoM.Scale(a.scale, a.scale)
	a.op.GeoM.Rotate(a.angle)
	a.op.GeoM.Scale(1, 1)
	a.op.GeoM.Translate(a.XY())
	a.op.GeoM.Translate(a.offsetX, a.offsetY)

	if a.isVisible {
		screen.DrawImage(a.sprites[a.animationIdx], a.op)
	}

	// debug
	// img := ebiten.NewImage(int(a.width), int(a.height))
	// img.Fill(color.RGBA{0, 255, 0, 255})
	// screen.DrawImage(img, a.op)
}

func (a *BaseArrow) CanDelete() bool {
	return a.canDelete
}
