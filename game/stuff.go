package game

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Stuff struct {
	name string
	*Rect
	*DamageStats
	angle float64

	magicArrowSound []byte

	sprite       *ebiten.Image
	arrowSprites []*ebiten.Image
	op           *ebiten.DrawImageOptions
	flip         bool

	attackTimer    float64
	attackCooldown float64
}

func NewStuff(
	name string,
	stuffSprite *ebiten.Image,
	arrowSprites []*ebiten.Image,
	sound []byte,
	attackCooldown float64,
	ds *DamageStats,
) *Stuff {
	s := &Stuff{
		name:            name,
		Rect:            &Rect{},
		angle:           0,
		sprite:          stuffSprite,
		op:              &ebiten.DrawImageOptions{},
		arrowSprites:    arrowSprites,
		attackCooldown:  attackCooldown,
		flip:            false,
		DamageStats:     ds,
		magicArrowSound: sound,
	}

	s.width = float64(stuffSprite.Bounds().Dx())
	s.height = float64(stuffSprite.Bounds().Dy())

	s.attackTimer = s.attackCooldown * FPS

	return s
}

func (s *Stuff) Update(cameraX, cameraY float64, g *Game) {
	s.x, s.y = g.player.CenterXY()

	centerX, centerY := s.CenterXY()
	centerX += cameraX
	centerY += cameraY

	cx, _ := ebiten.CursorPosition()
	if cx > int(s.x) {
		s.flip = false
	}
	if cx < int(s.x) {
		s.flip = true
	}

	// calc angle for weapon rotation
	cx, cy := ebiten.CursorPosition()
	s.angle = math.Atan2(float64(cy)-centerY, float64(cx)-centerX)

	s.attackTimer++
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) && s.attackTimer >= s.attackCooldown*FPS {
		g.audioPlayer.PlaySound(s.magicArrowSound)
		arrow := NewMagicArrow(
			s.arrowSprites,
			nil,
			s.x,
			s.y,
			s.angle,
			1.5,
			0,
			6,
			s.DamageStats,
			0.5,
		)
		s.attackTimer = 0
		g.arrows = append(g.arrows, arrow)
	}

}

func (s *Stuff) Draw(screen *ebiten.Image, p *Player) {
	s.op.GeoM.Reset()

	// scale and rotate
	s.op.GeoM.Translate(10, -13)

	if s.flip {
		s.op.GeoM.Scale(-1, 1)
	} else if !s.flip {
		s.op.GeoM.Scale(1, 1)
	}

	s.op.GeoM.Scale(BowScale, BowScale)
	s.op.GeoM.Scale(1, 1)
	s.op.GeoM.Translate(p.CenterXY())
	screen.DrawImage(s.sprite, s.op)

}

func (s *Stuff) GetIcon() *ebiten.Image {
	return s.sprite
}

func (s *Stuff) GetName() string {
	return s.name
}
