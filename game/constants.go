package game

import "image/color"

var UIColor = color.RGBA{163, 136, 82, 150}

// characters
const (
	Angel = iota
	BigDemon
	BigZombie
	Doc
	DwarfF
	DwarfM
	ElfF
	ElfM
	Goblin
	IceZombie
	Imp
	KnightF
	KnightM
	LizardF
	LizardM
	MaskedOrk
	Muddy
	Necromancer
	Ogre
	OrcShaman
	OrcWarrior
	PumpkinDude
	Skeleton
	Slug
	Swampy
	TinySlug
	TinyZombie
	WizzardF
	WizzardM
	Wogol
	Zombie
)

// potions
const (
	Red = iota
	Green
	Yellow
	Blue
)

const (
	FireArrow = iota
	IceArrow
	ShadowArrow
)

// TileTypes
const (
	// player
	PlayerType = 34
	// items
	RedPotionType  = 37
	BluePotionType = 35
	CoinType       = 20
	// chests
	ChestType      = 10
	MimicChestType = 13

	// npcs
	DwarfMType  = 31
	DwarfFType  = 30
	LizardFType = 65
	LizardMType = 66

	// enemies
	SkeletonType    = 74
	ImpType         = 60
	TinyZombieType  = 79
	MuddyType       = 68
	GoblinType      = 57
	BigDemonType    = 1
	AngelType       = 0
	DocType         = 24
	PumpkinDudeType = 73
	OrcShamanType   = 71
	OrcWarriorType  = 72
	SlugType        = 76
	SwampyType      = 77
	TinySlugType    = 78
	WogolType       = 156
	ZombieType      = 157
	IceZombieType   = 59
	MaskedOrkType   = 67
	NecromancerType = 69

	// walls
	WallTypeStart = 80
	WallTypeEnd   = 126

	// Torch
	TorchType = 158

	// floor
	BaseFloorType = 43

	// level exit
	LevelExitType  = 58
	LevelEnterType = 51
)

// general info
const (
	FPS          = 60
	ScreenWidth  = 1200
	ScreenHeight = 800

	CameraTreshholdX = 500
	CameraTreshholdY = 300

	// scaling
	LevelScale  = 3
	PlayerScale = 3
	ItemScale   = 3
	BowScale    = 2
	ArrowScale  = 1.5
	SwordScale  = 1.5
	TorchScale  = 2

	// tile
	TileSize  = 16 * LevelScale
	TileTypes = 159

	// Inventory
	InventoryIconScale = 2.5
	InventoryIconSize  = 16 * InventoryIconScale
	MaxSlotsInRow      = 9

	// player sprite gap on top
	GAP = 36

	// audio
	SampleRate = 48000
)
