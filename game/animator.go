package game

type Animator struct {
	animationIdx      int
	animationTimer    float64
	animationCooldown float64
	maxSprites        int
}

func NewAnimator(animCooldown float64, maxSprites int) *Animator {
	return &Animator{
		animationIdx:      0,
		animationTimer:    0,
		animationCooldown: animCooldown,
		maxSprites:        maxSprites,
	}
}

func (a *Animator) UpdateAnimIdx() {
	a.animationTimer++
	if a.animationTimer >= a.animationCooldown*FPS {
		a.animationIdx++
		a.animationTimer = 0
	}

	if a.animationIdx >= a.maxSprites {
		a.animationIdx = 0
	}
}
