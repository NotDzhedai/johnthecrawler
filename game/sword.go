package game

import (
	"fmt"
	"image/color"
	"log"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
)

type Sword struct {
	name string
	*Rect
	*DamageStats

	swordHitSound []byte

	angle     float64
	flip      bool
	canAttack bool

	sprite *ebiten.Image
	op     *ebiten.DrawImageOptions

	attackTimer    float64
	attackCooldown float64
}

func NewSword(name string, sprite *ebiten.Image, sound []byte, attackCooldown float64, ds *DamageStats) *Sword {
	s := &Sword{
		name:           name,
		Rect:           &Rect{},
		angle:          0,
		sprite:         sprite,
		op:             &ebiten.DrawImageOptions{},
		flip:           false,
		canAttack:      false,
		attackTimer:    0,
		attackCooldown: attackCooldown,
		DamageStats:    ds,
		swordHitSound:  sound,
	}

	s.width = float64(s.sprite.Bounds().Dx()) * SwordScale
	s.height = float64(s.sprite.Bounds().Dy()) * SwordScale

	return s
}

func (s *Sword) Update(cameraX, cameraY float64, g *Game) {
	s.x, s.y = g.player.CenterXY()
	centerX, centerY := s.CenterXY()

	centerX += cameraX
	centerY += cameraY

	cx, cy := ebiten.CursorPosition()

	cRect := s.Rect
	if cx > int(s.x) {
		s.flip = false
	}
	if cx < int(s.x) {
		cRect.x = s.x - s.width
		s.flip = true
	}
	if cy < int(s.y) {
		cRect.y = s.y - s.height
	}

	s.angle = math.Atan2(float64(cy)-centerY, float64(cx)-centerX)

	s.attackTimer++
	if s.attackTimer >= s.attackCooldown*FPS {
		s.canAttack = true
		s.attackTimer = 0
	}

	for _, enemy := range g.currentLevel.enemies {
		if cRect.IsCollide(enemy.Rect) && enemy.isAlive && s.canAttack {
			g.audioPlayer.PlaySound(s.swordHitSound)
			ct := s.makeDamage(enemy)
			s.canAttack = false
			g.combatTexts = append(g.combatTexts, ct)
		}
	}
}

func (s *Sword) makeDamage(enemy *Enemy) *CombatText {
	damage := s.DealDamage(enemy.CharacterStats)
	log.Printf("Player attacked %s for %d\n", enemy.name, damage)

	return NewCombatText(
		enemy.CenterX(),
		enemy.y-10,
		fmt.Sprint(damage),
		0.5,
		25,
		color.RGBA{255, 0, 0, 255},
	)
}

func (s *Sword) Draw(screen *ebiten.Image, p *Player) {
	s.op.GeoM.Reset()
	// scale and rotate

	s.op.GeoM.Rotate(math.Pi / 2)
	s.op.GeoM.Translate(s.width, 0)
	s.op.GeoM.Translate(20, -4)

	if s.flip {
		s.op.GeoM.Scale(1, -1)
	} else if !s.flip {
		s.op.GeoM.Scale(1, 1)
	}

	s.op.GeoM.Scale(SwordScale, SwordScale)
	s.op.GeoM.Rotate(s.angle)
	s.op.GeoM.Scale(1, 1)

	s.op.GeoM.Translate(p.CenterXY())

	screen.DrawImage(s.sprite, s.op)

}

func (s *Sword) GetIcon() *ebiten.Image {
	return s.sprite
}

func (s *Sword) GetName() string {
	return s.name
}
