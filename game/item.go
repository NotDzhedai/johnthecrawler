package game

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type Item interface {
	Update(float64, float64, *Level, *Player)
	Draw(*ebiten.Image)
	CanDelete() bool
}

type baseItem struct {
	*Rect
	*Animator
	isAnimated bool

	sprites   []*ebiten.Image
	op        *ebiten.DrawImageOptions
	isVisible bool
	toDelete  bool

	price int
}

func newBaseItem(x, y float64, isAnimated bool, sprites []*ebiten.Image, price int) *baseItem {
	i := &baseItem{
		Rect: &Rect{
			x:      x,
			y:      y,
			width:  float64(sprites[0].Bounds().Dx()) * ItemScale,
			height: float64(sprites[0].Bounds().Dy()) * ItemScale,
		},
		Animator:   NewAnimator(0.3, len(sprites)),
		isAnimated: isAnimated,
		sprites:    sprites,
		op:         &ebiten.DrawImageOptions{},
		isVisible:  false,
		toDelete:   false,
		price:      price,
	}

	return i
}

func (i *baseItem) Update(cameraX, cameraY float64, l *Level, p *Player) {
	i.x += cameraX
	i.y += cameraY

	visible := DDA(p.Rect, i.Rect, l, p.lineOfSight)
	if visible {
		i.isVisible = true
	}

	if i.isAnimated {
		i.Animator.UpdateAnimIdx()
	}
}

func (i *baseItem) Draw(screen *ebiten.Image) {
	i.op.GeoM.Reset()

	i.op.GeoM.Scale(ItemScale, ItemScale)
	i.op.GeoM.Scale(1, 1)
	i.op.GeoM.Translate(i.XY())

	if i.isVisible {
		screen.DrawImage(i.sprites[i.animationIdx], i.op)
	}

}

func (i *baseItem) CanDelete() bool {
	return i.toDelete
}
