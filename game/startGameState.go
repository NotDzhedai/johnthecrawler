package game

import (
	"fmt"
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"github.com/tinne26/etxt"
)

type heroChoice int

const (
	knightChoice heroChoice = iota + 1
	elfChoice
	wizzardChoice
)

const (
	StartGameMenuScale            = 10
	GapBetweenImageAndDescription = 60
)

type startGameState struct {
	assets            *Assets
	animationIdx      int
	animationTimer    float64
	animationCooldown float64

	currentChoice heroChoice

	knight  *Rect
	elf     *Rect
	wizzard *Rect

	startGameButton *Rect

	op *ebiten.DrawImageOptions
}

func NewStartGameState(a *Assets) *startGameState {
	sgs := &startGameState{
		assets:            a,
		animationIdx:      0,
		animationTimer:    0,
		animationCooldown: 0.2,
		currentChoice:     0,
		op:                &ebiten.DrawImageOptions{},
	}

	// knight button rect
	sgs.knight = &Rect{
		y:      150,
		width:  float64(a.CharacterSprites[KnightM][0][0].Bounds().Dx() * StartGameMenuScale),
		height: float64(a.CharacterSprites[KnightM][0][0].Bounds().Dy() * StartGameMenuScale),
	}
	sgs.knight.x = ScreenWidth/4 - (sgs.knight.width / 2)
	// elf button rect
	sgs.elf = &Rect{
		y:      150,
		width:  float64(a.CharacterSprites[ElfM][0][0].Bounds().Dx() * StartGameMenuScale),
		height: float64(a.CharacterSprites[ElfM][0][0].Bounds().Dy() * StartGameMenuScale),
	}
	sgs.elf.x = ScreenWidth/2 - (sgs.elf.width / 2)
	// wizzard button rect
	sgs.wizzard = &Rect{
		y:      150,
		width:  float64(a.CharacterSprites[WizzardM][0][0].Bounds().Dx() * StartGameMenuScale),
		height: float64(a.CharacterSprites[WizzardM][0][0].Bounds().Dy() * StartGameMenuScale),
	}
	sgs.wizzard.x = (ScreenWidth/2 + ScreenWidth/4) - sgs.knight.width/2
	// start game button rect
	sgs.startGameButton = &Rect{
		x:      ScreenWidth/2 - 100,
		y:      ScreenHeight - 100,
		width:  200,
		height: 50,
	}

	return sgs
}

func (sgs *startGameState) enter(args ...any) {}

func (sgs *startGameState) exit() {}

func (sgs *startGameState) update(g *Game) {
	cx, cy := ebiten.CursorPosition()

	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
		cursorRect := &Rect{x: float64(cx), y: float64(cy), width: 1, height: 1}
		if cursorRect.IsCollide(sgs.knight) {
			sgs.currentChoice = knightChoice
		}
		if cursorRect.IsCollide(sgs.elf) {
			sgs.currentChoice = elfChoice
		}
		if cursorRect.IsCollide(sgs.wizzard) {
			sgs.currentChoice = wizzardChoice
		}
		if cursorRect.IsCollide(sgs.startGameButton) {
			var p *Player
			switch sgs.currentChoice {
			case knightChoice:
				p = NewPlayer(0, 0, NewStats(&CharacterStatsConfig{
					Health: 150,
					Mana:   50,
					Speed:  4,
				}), sgs.assets.CharacterSprites[KnightM])
				p.AddWeapon(NewSword("Beginner's sword", sgs.assets.SwordSprite, sgs.assets.SwordHitSound, 1, NewDamageStats(30, 25, 30)))
			case elfChoice:
				p = NewPlayer(0, 0, NewStats(&CharacterStatsConfig{
					Health: 100,
					Mana:   100,
					Speed:  4,
				}), sgs.assets.CharacterSprites[ElfM])
				p.AddWeapon(NewBow(
					"Wooden bow",
					sgs.assets,
					0.5,
					NewDamageStats(200, 25, 25)),
				)
			case wizzardChoice:
				p = NewPlayer(0, 0, NewStats(&CharacterStatsConfig{
					Health: 90,
					Mana:   150,
					Speed:  3,
				}), sgs.assets.CharacterSprites[WizzardM])
				p.AddWeapon(NewStuff(
					"Fire stuff",
					sgs.assets.StuffSprite,
					sgs.assets.MagicArrows[ShadowArrow],
					sgs.assets.MagicArrowSound,
					1,
					NewDamageStats(25, 20, 25),
				))
			}
			g.player = p
			g.SetCurrentLevel(sgs.assets, 1)
			g.sm.Change(MainStateType)
		}
	}

	sgs.animationTimer++
	if sgs.animationTimer >= sgs.animationCooldown*FPS {
		sgs.animationIdx++
		sgs.animationTimer = 0
	}

	if sgs.animationIdx >= len(sgs.assets.CharacterSprites[0][IdleAnimation]) {
		sgs.animationIdx = 0
	}
}

func (sgs *startGameState) draw(screen *ebiten.Image, g *Game) {
	screen.Fill(color.Black)

	g.textRenderer.SetTarget(screen)
	g.textRenderer.SetColor(color.White)
	g.textRenderer.SetSizePx(40)
	g.textRenderer.SetAlign(etxt.Top, etxt.XCenter)

	g.textRenderer.Draw("Choose your class", ScreenWidth/2, 40)
	g.textRenderer.SetSizePx(20)
	g.textRenderer.SetAlign(etxt.Top, etxt.Left)

	// Knight
	if sgs.currentChoice == knightChoice {
		vector.DrawFilledCircle(
			screen,
			float32(sgs.knight.CenterX()),
			float32(sgs.knight.CenterY()+35),
			150,
			color.RGBA{77, 169, 240, 100},
			true,
		)
	}
	sgs.op.GeoM.Reset()
	sgs.op.GeoM.Scale(StartGameMenuScale, StartGameMenuScale)
	sgs.op.GeoM.Scale(1, 1)
	sgs.op.GeoM.Translate(sgs.knight.x, sgs.knight.y)
	screen.DrawImage(sgs.assets.CharacterSprites[KnightM][IdleAnimation][sgs.animationIdx], sgs.op)
	g.textRenderer.Draw(
		getDescriptionText(150, 50, 4, "sword"),
		int(sgs.knight.x),
		int(sgs.knight.y+sgs.knight.height+GapBetweenImageAndDescription),
	)

	// Elf
	if sgs.currentChoice == elfChoice {
		vector.DrawFilledCircle(
			screen,
			float32(sgs.elf.CenterX()),
			float32(sgs.elf.CenterY()+35),
			150,
			color.RGBA{120, 237, 116, 100},
			true,
		)
	}
	sgs.op.GeoM.Reset()
	sgs.op.GeoM.Scale(StartGameMenuScale, StartGameMenuScale)
	sgs.op.GeoM.Scale(1, 1)
	sgs.op.GeoM.Translate(sgs.elf.x, sgs.elf.y)
	screen.DrawImage(sgs.assets.CharacterSprites[ElfM][IdleAnimation][sgs.animationIdx], sgs.op)
	g.textRenderer.Draw(
		getDescriptionText(100, 100, 5, "bow"),
		int(sgs.elf.x),
		int(sgs.elf.y+sgs.elf.height+GapBetweenImageAndDescription),
	)

	// Wizzard
	if sgs.currentChoice == wizzardChoice {
		vector.DrawFilledCircle(
			screen,
			float32(sgs.wizzard.CenterX()),
			float32(sgs.wizzard.CenterY()+35),
			150,
			color.RGBA{221, 140, 230, 100},
			true,
		)
	}
	sgs.op.GeoM.Reset()
	sgs.op.GeoM.Scale(StartGameMenuScale, StartGameMenuScale)
	sgs.op.GeoM.Scale(1, 1)
	sgs.op.GeoM.Translate(sgs.wizzard.x, sgs.wizzard.y)
	screen.DrawImage(sgs.assets.CharacterSprites[WizzardM][IdleAnimation][sgs.animationIdx], sgs.op)
	g.textRenderer.Draw(
		getDescriptionText(90, 150, 3, "stuff"),
		int(sgs.wizzard.x),
		int(sgs.wizzard.y+sgs.wizzard.height+GapBetweenImageAndDescription),
	)

	// Start game button
	if sgs.currentChoice != 0 {
		g.textRenderer.SetAlign(etxt.Top, etxt.XCenter)
		g.textRenderer.SetColor(color.Black)
		sgs.op.GeoM.Reset()
		vector.DrawFilledRect(
			screen,
			float32(sgs.startGameButton.x),
			float32(sgs.startGameButton.y),
			float32(sgs.startGameButton.width),
			float32(sgs.startGameButton.height),
			color.RGBA{225, 232, 35, 200},
			true,
		)

		g.textRenderer.Draw("Start game", int(sgs.startGameButton.x+100), int(sgs.startGameButton.y+10))
	}
}

func getDescriptionText(hp, mp, speed int, sw string) string {
	return fmt.Sprintf("HP: %d\nMP: %d\nSpeed: %d\nStarting weapon: %s", hp, mp, speed, sw)
}
