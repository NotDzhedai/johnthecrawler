package game

import "github.com/hajimehoshi/ebiten/v2"

type Weapon interface {
	Update(cameraX, cameraY float64, g *Game)
	Draw(screen *ebiten.Image, p *Player)
	GetIcon() *ebiten.Image
	GetName() string
}
