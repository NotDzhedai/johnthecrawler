package game

import "github.com/hajimehoshi/ebiten/v2"

type Coin interface {
	Item
}

type baseCoin struct {
	*baseItem
	name  string
	value int
}

func newBaseCoin(name string, x, y float64, value int, sprites []*ebiten.Image) *baseCoin {
	i := newBaseItem(x, y, true, sprites, 0)
	c := &baseCoin{
		name:     name,
		baseItem: i,
		value:    value,
	}
	// place in the center of rect
	c.Rect.x, c.Rect.y = i.Rect.CenterXY()
	return c
}

func (c *baseCoin) Update(cameraX, cameraY float64, l *Level, p *Player) {

	c.baseItem.Update(cameraX, cameraY, l, p)

	if c.Rect.IsCollide(p.Rect) {
		p.inventory.gold += c.value
		c.toDelete = true
	}
}

func NewLittleCoin(x, y float64, sprites []*ebiten.Image) Coin {
	return newBaseCoin("Little coin", x, y, 10, sprites)
}
