package game

import "github.com/hajimehoshi/ebiten/v2"

type BowArrow struct {
	*BaseArrow
}

func NewBowArrow(image *ebiten.Image, sound []byte, x, y, angle, scale, rotation, speed float64, ds *DamageStats) *BowArrow {
	ba := &BowArrow{
		BaseArrow: NewBaseArrow([]*ebiten.Image{image}, sound, x, y, angle, scale, rotation, speed, ds, false),
	}

	ba.offsetX = -(ba.width / 2)
	ba.offsetY = 0

	return ba
}

func (ba *BowArrow) Update(cameraX, cameraY float64, l *Level, audioPlayer *AudioPlayer) *CombatText {
	ba.BaseArrow.Update(cameraX, cameraY)

	for _, obstacle := range l.obstacleTiles {
		if ba.IsCollide(obstacle.Rect) {
			audioPlayer.PlaySound(ba.hitSound)
			ba.canDelete = true
		}
	}

	for _, enemy := range l.enemies {
		if ba.IsCollide(enemy.Rect) && enemy.isAlive {
			ct := ba.MakeDamage(enemy)
			audioPlayer.PlaySound(ba.hitSound)
			ba.canDelete = true
			return ct
		}
	}

	return nil
}
