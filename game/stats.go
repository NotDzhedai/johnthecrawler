package game

import (
	"math/rand"
)

type DamageStats struct {
	baseDamage int
	critChance float64
	critPower  float64
}

func NewDamageStats(baseDamage int, critC, critP float64) *DamageStats {
	return &DamageStats{
		baseDamage: baseDamage,
		critChance: critC,
		critPower:  critP,
	}
}

func (ds *DamageStats) DealDamage(stats *CharacterStats) int {
	isCrit := rand.Float64() > (100-ds.critChance)/100

	damage := ds.baseDamage
	if isCrit {
		damage = int(float64(ds.baseDamage) * (1 + ds.critPower/100))
	}

	// TODO calc with enemy armor...

	stats.health -= damage

	return damage
}

type CharacterStatsConfig struct {
	Speed          float64
	Health         int
	Mana           int
	BaseDamage     int
	CritChance     float64
	CritPower      float64
	AttackCooldown float64
}

type CharacterStats struct {
	*DamageStats

	speed          float64
	health         int
	maxHealth      int
	mana           int
	maxMana        int
	isAlive        bool
	attackCooldown float64
}

func NewStats(cfg *CharacterStatsConfig) *CharacterStats {
	return &CharacterStats{
		health:         cfg.Health,
		maxHealth:      cfg.Health,
		mana:           cfg.Mana,
		maxMana:        cfg.Mana,
		speed:          cfg.Speed,
		isAlive:        true,
		DamageStats:    NewDamageStats(cfg.BaseDamage, cfg.CritChance, cfg.CritPower),
		attackCooldown: cfg.AttackCooldown,
	}
}

func (cs *CharacterStats) AddHP(value int) {
	cs.health += value
	if cs.health > cs.maxHealth {
		cs.health = cs.maxHealth
	}
}

func (cs *CharacterStats) AddMP(value int) {
	cs.mana += value
	if cs.mana > cs.maxMana {
		cs.mana = cs.maxMana
	}
}

func (cs *CharacterStats) IsAlive() bool {
	if cs.health <= 0 {
		cs.health = 0
		cs.isAlive = false
	}

	return cs.isAlive
}
