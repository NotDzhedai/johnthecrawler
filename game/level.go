package game

import (
	"fmt"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"golang.org/x/exp/slices"
)

type Tile struct {
	*Rect

	tileType   int
	tileSprite *ebiten.Image
	isObstacle bool
}

type LevelConfig struct {
	Name      string
	LevelData [][]int
	Assets    *Assets
}

type Level struct {
	name   string
	op     *ebiten.DrawImageOptions
	assets *Assets

	shadowImage   *ebiten.Image
	triangleImage *ebiten.Image

	tiles         [][]*Tile
	obstacleTiles []*Tile

	potions []Potion
	coins   []Coin
	enemies []*Enemy
	npcs    []Npc
	torches []*Torch
	// TODO boss *Boss

	levelNumber int

	exitTile  *Tile
	enterTile *Tile

	currentOffsetX float64
	currentOffsetY float64
}

func NewLevel(cfg *LevelConfig, g *Game, levelNumber int) (*Level, error) {
	l := &Level{
		name:           cfg.Name,
		assets:         cfg.Assets,
		op:             &ebiten.DrawImageOptions{},
		enemies:        make([]*Enemy, 0),
		npcs:           make([]Npc, 0),
		coins:          make([]Coin, 0),
		potions:        make([]Potion, 0),
		torches:        make([]*Torch, 0),
		shadowImage:    ebiten.NewImage(ScreenWidth, ScreenHeight),
		triangleImage:  ebiten.NewImage(ScreenWidth, ScreenHeight),
		currentOffsetX: 0,
		currentOffsetY: 0,
		levelNumber:    levelNumber,
	}

	l.tiles = make([][]*Tile, len(cfg.LevelData))
	for y := range l.tiles {
		l.tiles[y] = make([]*Tile, len(cfg.LevelData[0]))
	}

	for y, row := range cfg.LevelData {
		for x, col := range row {
			l.processTile(x, y, col, g.player, g.maxLevel)
		}
	}

	return l, nil
}

func (l *Level) processTile(x, y, tileType int, p *Player, maxLevelNumber int) {

	//  empty tile
	if tileType == -1 {
		l.tiles[y][x] = &Tile{tileType: -1}
		return
	}

	// not empty tile
	image := l.assets.TileSprites[tileType]
	tile := &Tile{
		tileType:   tileType,
		tileSprite: image,
		isObstacle: false,
		Rect: &Rect{
			x:      float64(x * TileSize),
			y:      float64(y * TileSize),
			width:  TileSize,
			height: TileSize,
		},
	}

	// Place player
	if tile.tileType == PlayerType {
		p.x = tile.x
		p.y = tile.y
		if l.levelNumber != 1 {
			// Level enter tile. Point to return to level above
			tile.tileSprite = l.assets.TileSprites[LevelEnterType]
			tile.tileType = LevelEnterType
		} else {
			// replace player placeholder with floor
			tile.tileSprite = l.assets.TileSprites[BaseFloorType]
		}
	}

	l.handleTileByType(tile, maxLevelNumber)

	l.tiles[y][x] = tile
}

// get tile by coordinates
func (l *Level) GetTile(tx, ty int) *Tile {
	x := int(math.Floor((float64(tx) - l.currentOffsetX) / TileSize))
	y := int(math.Floor((float64(ty) - l.currentOffsetY) / TileSize))
	if x < 0 || x >= len(l.tiles[0]) || y < 0 || y >= len(l.tiles) {
		return nil
	}

	return l.tiles[y][x]
}

func (l *Level) Update(cameraX, cameraY float64, g *Game) {

	for _, row := range l.tiles {
		for _, t := range row {
			if t.tileType == -1 {
				continue
			}
			t.x += cameraX
			t.y += cameraY
		}
	}

	l.currentOffsetX += cameraX
	l.currentOffsetY += cameraY

	// torches
	for _, t := range l.torches {
		t.Update(cameraX, cameraY, l, g.player)
	}

	// enemies
	for _, enemy := range l.enemies {
		ct := enemy.Update(cameraX, cameraY, l, g.player)
		if ct != nil {
			g.combatTexts = append(g.combatTexts, ct)
		}
	}

	// npcs
	for _, n := range l.npcs {
		n.Update(cameraX, cameraY, g)
	}

	// items
	for _, coin := range l.coins {
		coin.Update(cameraX, cameraY, l, g.player)
	}
	for _, pot := range l.potions {
		pot.Update(cameraX, cameraY, l, g.player)
	}

	// exit and enter tiles
	if l.exitTile != nil && l.exitTile.IsCollide(g.player.Rect) &&
		inpututil.IsKeyJustPressed(ebiten.KeyG) {
		g.SetCurrentLevel(l.assets, l.levelNumber+1)
	}

	if l.enterTile != nil && l.enterTile.IsCollide(g.player.Rect) &&
		inpututil.IsKeyJustPressed(ebiten.KeyG) {
		g.SetCurrentLevel(l.assets, l.levelNumber-1)
	}

	l.Clear()
}

func (l *Level) Clear() {
	l.enemies = slices.DeleteFunc(l.enemies, func(e *Enemy) bool {
		return !e.isAlive
	})
	l.npcs = slices.DeleteFunc(l.npcs, func(n Npc) bool {
		return !n.IsAlive()
	})
	l.coins = slices.DeleteFunc(l.coins, func(c Coin) bool {
		return c.CanDelete()
	})
	l.potions = slices.DeleteFunc(l.potions, func(p Potion) bool {
		return p.CanDelete()
	})
}

func (l *Level) Draw(screen *ebiten.Image, p *Player, d *Debug) {
	l.shadowImage.Fill(color.Black)

	cx, cy := p.CenterXY()
	rays := l.rayCasting(cx, cy, p.lineOfSight, 0.015)
	l.DrawTriangles(cx, cy, rays)

	// Draw tiles
	for _, row := range l.tiles {
		for _, t := range row {
			if t.tileType == -1 {
				continue
			}
			l.op.GeoM.Reset()
			l.op.GeoM.Scale(LevelScale, LevelScale)
			l.op.GeoM.Scale(1, 1)
			l.op.GeoM.Translate(t.XY())

			l.op.ColorScale.Reset()
			screen.DrawImage(t.tileSprite, l.op)
		}
	}

	// torch
	for _, t := range l.torches {
		t.Draw(screen, l, d)
	}
	// enemies
	for _, enemy := range l.enemies {
		enemy.Draw(screen, d)
	}
	// npcs
	for _, n := range l.npcs {
		n.Draw(screen)
	}
	// items
	for _, coin := range l.coins {
		coin.Draw(screen)
	}
	for _, potion := range l.potions {
		potion.Draw(screen)
	}

	// make all dark outside line of sight
	op := &ebiten.DrawImageOptions{}
	op.ColorScale.ScaleAlpha(0.7)
	screen.DrawImage(l.shadowImage, op)

	// draw rays
	if d.DrawPlayerRays {
		for _, r := range rays {
			vector.StrokeLine(screen, float32(r.X1), float32(r.Y1), float32(r.X2), float32(r.Y2), 1, color.RGBA{255, 255, 0, 150}, true)
		}
		ebitenutil.DebugPrintAt(screen, fmt.Sprint("Player rays: ", len(rays)), 20, 90)
	}

}

func (l *Level) DrawTriangles(cx, cy float64, rays []Line) []Line {

	// Subtract ray triangles from shadow
	opt := &ebiten.DrawTrianglesOptions{}
	opt.Address = ebiten.AddressRepeat
	opt.Blend = ebiten.BlendSourceOut
	for i, line := range rays {
		nextLine := rays[(i+1)%len(rays)]

		// Draw triangle of area between rays
		v := rayVertices(float64(cx), float64(cy), nextLine.X2, nextLine.Y2, line.X2, line.Y2)
		l.shadowImage.DrawTriangles(v, []uint16{0, 1, 2}, l.triangleImage, opt)
	}

	return rays
}

// processes tiles by type and fills the level with items, enemies, etc
func (l *Level) handleTileByType(tile *Tile, maxLevelNumber int) {
	switch tileType := tile.tileType; {
	// ! Level interactive tiles
	// Walls
	case tileType >= WallTypeStart && tileType <= WallTypeEnd:
		tile.isObstacle = true
		l.obstacleTiles = append(l.obstacleTiles, tile)
	// TODO Door

	// Level Exit
	case tileType == LevelExitType:
		if l.levelNumber != maxLevelNumber {
			// set exit tile if current level is not max level
			l.exitTile = tile
		} else {
			// make exit tile floor if current level is max level
			tile.tileSprite = l.assets.TileSprites[BaseFloorType]
		}
	case tileType == LevelEnterType:
		l.enterTile = tile
	// Coins
	case tileType == CoinType:
		coin := NewLittleCoin(tile.x, tile.y, l.assets.CoinSprites)
		l.coins = append(l.coins, coin)
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	// Potions
	case tileType == RedPotionType:
		potion := NewRedPotion(tile.x, tile.y, 10, l.assets.PotionSprites[Red])
		l.potions = append(l.potions, potion)
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	case tileType == BluePotionType:
		potion := NewBluePotion(tile.x, tile.y, 10, l.assets.PotionSprites[Blue])
		l.potions = append(l.potions, potion)
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	// ! Torch
	case tileType == TorchType:
		torch := NewTorch(tile.x, tile.y, 200, l.assets.TorchSprites)
		l.torches = append(l.torches, torch)
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	// ! Npcs
	case tileType == DwarfFType:
		l.npcs = append(l.npcs, Isolda(tile.x, tile.y, l.assets))
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	case tileType == DwarfMType:
		l.npcs = append(l.npcs, Robert(tile.x, tile.y, l.assets))
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	case tileType == LizardFType:
		l.npcs = append(l.npcs, Ksenia(tile.x, tile.y, l.assets))
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	case tileType == LizardMType:
		l.npcs = append(l.npcs, Arnold(tile.x, tile.y, l.assets))
		tile.tileSprite = l.assets.TileSprites[BaseFloorType]
	// ! Enemies
	case tileType == ImpType:
		l.addEnemy("Imp", l.assets, Imp, &CharacterStatsConfig{
			Speed:          6,
			Health:         50,
			BaseDamage:     5,
			CritChance:     20,
			CritPower:      25,
			AttackCooldown: 1,
		}, tile)
	case tileType == SkeletonType:
		l.addEnemy("Skeleton", l.assets, Skeleton, &CharacterStatsConfig{
			Speed:          3,
			Health:         100,
			BaseDamage:     20,
			CritChance:     25,
			CritPower:      20,
			AttackCooldown: 1,
		}, tile)

	case tileType == GoblinType:
		l.addEnemy("Goblin warrior", l.assets, Goblin, &CharacterStatsConfig{
			Speed:          4,
			Health:         80,
			BaseDamage:     10,
			CritChance:     20,
			CritPower:      25,
			AttackCooldown: 1,
		}, tile)

	case tileType == MuddyType:
		l.addEnemy("Muddy", l.assets, Muddy, &CharacterStatsConfig{
			Speed:          2,
			Health:         150,
			BaseDamage:     30,
			CritChance:     30,
			CritPower:      30,
			AttackCooldown: 1.5,
		}, tile)

	case tileType == TinyZombieType:
		l.addEnemy("Tiny zombie", l.assets, TinyZombie, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == AngelType:
		l.addEnemy("Angel", l.assets, Angel, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == DocType:
		l.addEnemy("Doc", l.assets, Doc, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == PumpkinDudeType:
		l.addEnemy("Pumpkin Dude", l.assets, PumpkinDude, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == OrcShamanType:
		l.addEnemy("Orc Shaman", l.assets, OrcShaman, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == OrcWarriorType:
		l.addEnemy("Orc Warrior", l.assets, OrcWarrior, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == SlugType:
		l.addEnemy("Slug", l.assets, Slug, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == SwampyType:
		l.addEnemy("Swampy", l.assets, Swampy, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == TinySlugType:
		l.addEnemy("Tiny Slug", l.assets, TinySlug, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == WogolType:
		l.addEnemy("Wogol", l.assets, Wogol, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == ZombieType:
		l.addEnemy("Zombie", l.assets, Zombie, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == IceZombieType:
		l.addEnemy("Ice Zombie", l.assets, IceZombie, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == MaskedOrkType:
		l.addEnemy("Masked Ork", l.assets, MaskedOrk, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
	case tileType == NecromancerType:
		l.addEnemy("Necromancer", l.assets, Necromancer, &CharacterStatsConfig{
			Speed:          4,
			Health:         70,
			BaseDamage:     10,
			CritChance:     40,
			CritPower:      50,
			AttackCooldown: 0.5,
		}, tile)
		// TODO boss
		// case tileType == BigDemonType:
		// 	if err := l.replaceWithEnemy(cfg, BigDemon, tile); err != nil {
		// 		return nil, err
		// 	}
	}
}

// add enemy to level and replace tile with floor
func (l *Level) addEnemy(name string, assets *Assets, charType int, charStatCfg *CharacterStatsConfig, tile *Tile) {
	mob := NewEnemy(name, tile.x, tile.y, NewStats(charStatCfg), assets.CharacterSprites[charType])
	l.enemies = append(l.enemies, mob)
	// replace player placeholder with floor
	tile.tileSprite = assets.TileSprites[BaseFloorType]
}
