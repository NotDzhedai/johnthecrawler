package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type tradeState struct {
	rect        Rect
	tradeWindow *ebiten.Image
	op          *ebiten.DrawImageOptions
	npc         Npc

	inventoryStartX float64
	inventoryStartY float64
}

func newTradeState(rect Rect) *tradeState {
	return &tradeState{
		rect:            rect,
		tradeWindow:     ebiten.NewImage(int(rect.width), int(rect.height)),
		op:              &ebiten.DrawImageOptions{},
		inventoryStartX: rect.x + 10 - InventoryIconSize,
		inventoryStartY: 300,
	}
}

func (ts *tradeState) enter(args ...any) {
	if len(args) != 2 {
		panic("wrong number og args in trade enter method")
	}

	npc, ok := args[0].(Npc)
	if !ok {
		panic("wrong 1 arg type in trade enter method")
	}
	ts.npc = npc

	game, ok := args[1].(*Game)
	if !ok {
		panic("wrong 2 arg type in trade enter method")
	}

	game.sm.states[InventoryStateType].enter(InventoryToTrade, ts.npc.GetInventory())

}

func (ts *tradeState) exit() {}

func (ts *tradeState) update(g *Game) {
	if inpututil.IsKeyJustPressed(ebiten.KeyT) || inpututil.IsKeyJustPressed(ebiten.KeyEscape) {
		g.sm.Change(MainStateType)
	}

	g.sm.states[InventoryStateType].update(g)

	// handle inventory clicks
	cx, cy := ebiten.CursorPosition()
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
		slot, idx := GetInventorySlot(
			float64(cx), float64(cy),
			ts.inventoryStartX, ts.inventoryStartY,
			ts.npc.GetInventory(),
		)
		if slot != nil {
			if g.player.inventory.gold >= slot.Price() {
				g.player.inventory.Add(slot)
				g.player.inventory.gold -= slot.Price()
				ts.npc.GetInventory().Delete(idx)
				ts.npc.GetInventory().gold += slot.Price()
			}
		}
	}
}

func (ts *tradeState) draw(screen *ebiten.Image, g *Game) {
	g.sm.states[MainStateType].draw(screen, g)
	g.sm.states[InventoryStateType].draw(screen, g)

	ts.tradeWindow.Fill(UIColor)

	// setup text renderer
	g.textRenderer.SetTarget(ts.tradeWindow)
	g.textRenderer.SetColor(color.Black)

	// player info
	DrawCharacterInfo(
		g.textRenderer,
		ts.op,
		ts.tradeWindow,
		ts.npc.GetIcon(),
		ts.npc.GetName(),
		10,
		ts.npc.GetStats(),
		ts.npc.GetInventory(),
	)

	// inventory header
	DrawInventoryHeader(g.textRenderer, ts.op, ts.tradeWindow, screen, ts.rect, ts.npc.GetInventory())

	// trade window
	ts.op.GeoM.Reset()
	ts.op.GeoM.Translate(ts.rect.XY())
	screen.DrawImage(ts.tradeWindow, ts.op)

	// draw nps inventory
	DrawInventory(
		ts.op,
		screen,
		ts.npc.GetInventory(),
		ts.inventoryStartX, ts.inventoryStartY)
}
