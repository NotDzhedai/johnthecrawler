package game

import (
	"fmt"
	"image/color"
	"log"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Enemy struct {
	name string
	*CharacterAnimator
	*Mover
	*CharacterStats

	isVisible bool

	attackRange float64

	isAggressive bool
	lineOfSight  float64

	passiveTimer        float64
	passiveMoveCooldown float64
	// slowly and randomly walks around the level
	passiveSpeed float64
	// When the enemy overtakes the player, their speed must be the same.
	// So that the enemy does not overtake the player,
	// and there are no breaks in the  enemy run animation
	chasingSpeed float64

	postAggresstionTimer    float64
	postAggresstionCooldown float64

	attackTimer float64
}

func NewEnemy(name string, x, y float64, stats *CharacterStats, sprites [][]*ebiten.Image) *Enemy {
	e := &Enemy{
		name:              name,
		CharacterAnimator: NewCharacterAnimator(x, y, sprites),
		CharacterStats:    stats,

		attackRange: 30,

		lineOfSight:  8 * TileSize,
		isAggressive: false,

		passiveTimer:        0,
		passiveMoveCooldown: 3,
		passiveSpeed:        1,

		postAggresstionTimer:    0,
		postAggresstionCooldown: 5, // if aggressive, enemy will run after you 5 more seconds
	}

	e.chasingSpeed = e.speed
	e.postAggresstionTimer = e.postAggresstionCooldown * FPS
	e.attackTimer = e.attackTimer * FPS

	return e
}

func (e *Enemy) Update(cameraX, cameraY float64, l *Level, p *Player) *CombatText {
	if !e.IsAlive() {
		return nil
	}

	visible := DDA(p.Rect, e.Rect, l, p.lineOfSight)
	if visible {
		e.isVisible = true
	}

	e.CharacterAnimator.Update(cameraX, cameraY, l)
	ct := e.ai(l, p)
	return ct
}

func (e *Enemy) Draw(screen *ebiten.Image, debug *Debug) {
	if e.isVisible {
		e.CharacterAnimator.Draw(screen, 0)
	}

	// debug
	if debug.Characters {
		vector.DrawFilledRect(
			screen, float32(e.x),
			float32(e.y),
			float32(e.width),
			float32(e.height),
			color.RGBA{0, 255, 0, 0},
			false)
	}

}

func (e *Enemy) ai(l *Level, p *Player) *CombatText {
	ex, ey := e.CenterXY()
	px, py := p.CenterXY()

	e.postAggresstionTimer++
	if e.postAggresstionTimer >= e.postAggresstionCooldown*FPS && e.isAggressive {
		e.isAggressive = e.canAttack(px, py, l)
		e.postAggresstionTimer = 0
	} else if !e.isAggressive {
		e.isAggressive = e.canAttack(px, py, l)
	}

	dx := 0.0
	dy := 0.0
	if e.isAggressive {
		if ex+e.speed > px {
			dx -= e.chasingSpeed
		}
		if ex < px {
			dx += e.chasingSpeed
		}
		if ey-e.speed > py {
			dy -= e.chasingSpeed
		}
		if ey < py {
			dy += e.chasingSpeed
		}
	} else {
		e.chasingSpeed = e.speed
		// dx, dy = e.passiveMove()
	}

	// check if enemy close enough to player to deal damage
	e.attackTimer++
	var ct *CombatText
	if e.inAttackRange(ex, ey, px, py) {
		if e.speed > p.speed {
			e.chasingSpeed = p.speed
		}
		dx = 0
		dy = 0
		ct = e.attack(p)
	}

	e.Move(dx, dy, e.CharacterAnimator, l.obstacleTiles)

	return ct
}

func (e *Enemy) inAttackRange(ex, ey, tx, ty float64) bool {
	return Distance(ex, ey, tx, ty) <= e.attackRange
}

func (e *Enemy) attack(p *Player) *CombatText {
	if e.attackTimer >= e.attackCooldown*FPS {
		damage := e.DealDamage(p.CharacterStats)
		e.attackTimer = 0
		ct := NewCombatText(
			p.CenterX(),
			p.y-10,
			fmt.Sprint(damage),
			1,
			20,
			color.RGBA{251, 90, 34, 255},
		)
		log.Printf("%s attacked player for %d\n", e.name, damage)
		return ct
	}

	return nil
}

func (e *Enemy) canAttack(targetX, targetY float64, l *Level) bool {
	// start pos
	startX, startY := e.CenterXY()

	// direction
	dirX := targetX - startX
	dirY := targetY - startY
	mag := 1 / math.Sqrt(float64(dirX*dirX+dirY*dirY))

	// normalized direction
	dirXN := float64(dirX) * mag
	dirYN := float64(dirY) * mag

	vRayUnitStepSizeX := math.Sqrt(1 + (dirYN/dirXN)*(dirYN/dirXN))
	vRayUnitStepSizeY := math.Sqrt(1 + (dirXN/dirYN)*(dirXN/dirYN))

	vMapCheckX := int(startX)
	vMapCheckY := int(startY)

	var vRayLen1DX, vRayLen1DY float64
	var vStepX, vStepY int

	if dirXN < 0 {
		vStepX = -1
		vRayLen1DX = (startX - float64(vMapCheckX)) * vRayUnitStepSizeX
	} else {
		vStepX = 1
		vRayLen1DX = (float64(vMapCheckX+1) - startX) * vRayUnitStepSizeX
	}
	if dirYN < 0 {
		vStepY = -1
		vRayLen1DY = (startY - float64(vMapCheckY)) * vRayUnitStepSizeY
	} else {
		vStepY = 1
		vRayLen1DY = (float64(vMapCheckY+1) - startY) * vRayUnitStepSizeY
	}

	tileFound := false
	distanceX, distanceY := 0.0, 0.0
	for !tileFound {
		if distanceX > e.lineOfSight && distanceY > e.lineOfSight {
			return false
		}
		// walk
		if vRayLen1DX < vRayLen1DY {
			vMapCheckX += vStepX
			distanceX = vRayLen1DX
			vRayLen1DX += vRayUnitStepSizeX
		} else {
			vMapCheckY += vStepY
			distanceY = vRayLen1DY
			vRayLen1DY += vRayUnitStepSizeY
		}

		if vMapCheckX >= 0 && vMapCheckX < ScreenWidth &&
			vMapCheckY >= 0 && vMapCheckY < ScreenHeight {
			t := l.GetTile(vMapCheckX, vMapCheckY)

			if t != nil && t.isObstacle {
				tileFound = true
			}

			if vMapCheckX == int(targetX) && vMapCheckY == int(targetY) {
				return true
			}

		}
	}

	return !tileFound
}
