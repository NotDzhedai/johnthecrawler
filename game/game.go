package game

import (
	"encoding/csv"
	"fmt"
	"image/color"
	"os"
	"strconv"

	_ "embed"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/tinne26/etxt"
	"github.com/tinne26/fonts/liberation/lbrtserif"
)

type Debug struct {
	Characters     bool
	Player         bool
	Camera         bool
	AllMap         bool
	DrawPlayerRays bool
	DrawTorchRays  bool
}

func (d *Debug) handleInputs() {
	if inpututil.IsKeyJustPressed(ebiten.Key0) {
		d.DrawPlayerRays = !d.DrawPlayerRays
	}
	if inpututil.IsKeyJustPressed(ebiten.Key9) {
		d.DrawTorchRays = !d.DrawTorchRays
	}
}

type AudioPlayer struct {
	aCtx *audio.Context
}

type Game struct {
	debug  *Debug
	levels map[int]*Level

	currentLevel *Level
	maxLevel     int

	textRenderer *etxt.Renderer
	audioPlayer  *AudioPlayer

	sm *StateMachine

	player *Player

	arrows      []Arrow
	combatTexts []*CombatText
}

func NewGame() (*Game, error) {
	assets, err := LoadAssets()
	if err != nil {
		return nil, err
	}

	// Create text renderer for battle text
	txtRenderer, err := newTextRenderer(color.RGBA{255, 0, 0, 255})
	if err != nil {
		return nil, err
	}

	g := &Game{
		sm: NewStateMachine(
			Rect{30, 100, 400, 400},
			Rect{(ScreenWidth / 2) - (800 / 2), ScreenHeight - 400, 800, 350},
			Rect{500, 100, 400, 400},
			assets,
		),
		debug: &Debug{
			Characters:     false,
			Player:         false,
			Camera:         false,
			AllMap:         false,
			DrawPlayerRays: false,
			DrawTorchRays:  false,
		},
		textRenderer: txtRenderer,
		arrows:       make([]Arrow, 0),
		combatTexts:  make([]*CombatText, 0),
		levels:       make(map[int]*Level),
		maxLevel:     2,
		audioPlayer:  &AudioPlayer{aCtx: audio.NewContext(SampleRate)},
	}

	return g, nil
}

func (a *AudioPlayer) PlaySound(sound []byte) {
	a.aCtx.NewPlayerFromBytes(sound).Play()

}

func (g *Game) SetCurrentLevel(assets *Assets, levelNumber int) error {
	l, ok := g.levels[levelNumber]
	if ok {

		if levelNumber < g.currentLevel.levelNumber {
			g.player.x = l.exitTile.x
			g.player.y = l.exitTile.y
		}
		if levelNumber > g.currentLevel.levelNumber {
			g.player.x = l.enterTile.x
			g.player.y = l.enterTile.y
		}
		g.currentLevel = l

		return nil
	}

	name := fmt.Sprintf("level_%d", levelNumber)
	levelData, err := loadLevelData(name)
	if err != nil {
		return err
	}

	level, err := NewLevel(&LevelConfig{
		Name:      name,
		LevelData: levelData,
		Assets:    assets,
	}, g, levelNumber)
	if err != nil {
		return err
	}

	g.levels[levelNumber] = level
	g.currentLevel = level

	return nil
}

func (g *Game) Layout(width, height int) (int, int) {
	return ScreenWidth, ScreenHeight
}

func (g *Game) Update() error {

	g.sm.Update(g)

	// for debug
	g.debug.handleInputs()

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	g.sm.Draw(screen, g)
	// debug
	g.textRenderer.SetTarget(screen)
	g.textRenderer.SetColor(color.White)
	g.textRenderer.SetSizePx(15)
	g.textRenderer.Draw(fmt.Sprintf("TPS %.1f", ebiten.ActualTPS()), 50, 50)
}

func loadCharactersSprites(characterModels []string) ([][][]*ebiten.Image, error) {
	sprites := [][][]*ebiten.Image{}
	animationTypes := []string{"idle", "run"}
	for _, char := range characterModels {
		charAnimations := [][]*ebiten.Image{}
		for _, animationType := range animationTypes {
			temp := []*ebiten.Image{}
			for i := 0; i < 4; i++ {
				s, _, err := ebitenutil.NewImageFromFile(
					fmt.Sprintf("assets/images/characters/%s/%s/%d.png", char, animationType, i),
				)
				if err != nil {
					return nil, err
				}
				temp = append(temp, s)
			}
			charAnimations = append(charAnimations, temp)
		}
		sprites = append(sprites, charAnimations)
	}

	return sprites, nil
}

func newTextRenderer(color color.Color) (*etxt.Renderer, error) {
	// load font library
	// fontLib := etxt.NewFontLibrary()
	// _, _, err := fontLib.ParseDirFonts("assets/fonts")
	// if err != nil {
	// 	return nil, err
	// }

	txtRenderer := etxt.NewStdRenderer()
	// txtRenderer.SetFont(fontLib.GetFont("Atari Regular"))
	txtRenderer.SetFont(lbrtserif.Font())
	glyphsCache := etxt.NewDefaultCache(10 * 1024 * 1024)
	txtRenderer.SetCacheHandler(glyphsCache.NewHandler())
	txtRenderer.SetColor(color)
	return txtRenderer, nil
}

// load level csv
func loadLevelData(levelName string) ([][]int, error) {
	file, err := os.Open(fmt.Sprintf("levels/%s.csv", levelName))
	if err != nil {
		return nil, err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	reader.FieldsPerRecord = -1
	data, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	result := make([][]int, len(data))

	for x, row := range data {
		for _, col := range row {
			tileType, err := strconv.Atoi(col)
			if err != nil {
				return nil, err
			}
			result[x] = append(result[x], tileType)
		}
	}

	return result, nil
}
