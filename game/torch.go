package game

import (
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Torch struct {
	*Rect
	lightRange float64
	op         *ebiten.DrawImageOptions
	sprites    []*ebiten.Image

	animationIdx      int
	animationTimer    float64
	animationCooldown float64

	restoreTimer    float64
	restoreCooldown float64
	restoreRange    float64
}

func NewTorch(x, y, lightRange float64, sprites []*ebiten.Image) *Torch {
	t := &Torch{
		Rect:              &Rect{x: x, y: y},
		lightRange:        lightRange,
		sprites:           sprites,
		op:                &ebiten.DrawImageOptions{},
		animationIdx:      0,
		animationTimer:    0,
		animationCooldown: 0.3,
		restoreTimer:      0,
		restoreCooldown:   5,
		restoreRange:      TileSize * 2,
	}

	t.width = float64(t.sprites[0].Bounds().Dx())
	t.height = float64(t.sprites[0].Bounds().Dy())

	return t
}

func (t *Torch) Update(cameraX, cameraY float64, l *Level, p *Player) {
	t.x += cameraX
	t.y += cameraY

	// restore mp and hp
	t.restoreTimer++
	if t.restoreTimer >= t.restoreCooldown*FPS &&
		Distance(t.CenterX(), t.CenterY(), p.CenterX(), p.CenterY()) < t.restoreRange {
		p.AddHP(10)
		p.AddMP(10)
		t.restoreTimer = 0
	}

	t.animationTimer++
	if t.animationTimer >= t.animationCooldown*FPS {
		t.animationIdx++
		t.animationTimer = 0
	}

	if t.animationIdx >= len(t.sprites) {
		t.animationIdx = 0
	}
}

func (t *Torch) Draw(screen *ebiten.Image, l *Level, d *Debug) {
	tx, ty := t.CenterXY()
	rays := l.rayCasting(tx, ty, t.lightRange, math.Pi/24)
	l.DrawTriangles(tx, ty, rays)

	if d.DrawTorchRays {
		for _, r := range rays {
			vector.StrokeLine(screen, float32(r.X1), float32(r.Y1), float32(r.X2), float32(r.Y2), 1, color.RGBA{255, 255, 0, 150}, true)
		}
	}

	t.op.GeoM.Reset()
	t.op.GeoM.Scale(TorchScale, TorchScale)
	t.op.GeoM.Scale(1, 1)

	t.op.GeoM.Translate(t.CenterX(), t.y)
	screen.DrawImage(t.sprites[t.animationIdx], t.op)
}
