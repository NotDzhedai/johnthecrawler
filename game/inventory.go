package game

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"golang.org/x/exp/slices"
)

type Usable interface {
	Use(*Player) bool
	Name() string
	Icon() *ebiten.Image
	Price() int
}

type Inventory struct {
	gold    int
	maxSize int
	slots   []Usable
}

func NewInventory() *Inventory {
	inv := &Inventory{
		maxSize: 30,
		slots:   make([]Usable, 0),
	}

	return inv
}

func (i *Inventory) Len() int {
	return len(i.slots)
}

func (i *Inventory) Add(item Usable) bool {
	if len(i.slots) >= i.maxSize {
		log.Println("max inventory size")
		return false
	}

	i.slots = append(i.slots, item)
	return true
}

func (i *Inventory) Delete(idx int) {
	i.slots = slices.Delete(i.slots, idx, idx+1)
}
