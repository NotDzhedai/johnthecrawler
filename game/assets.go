package game

import (
	"bufio"
	"bytes"
	"fmt"
	"image"
	"io"
	"os"
	"path"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/audio/mp3"
	"github.com/hajimehoshi/ebiten/v2/audio/vorbis"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type Assets struct {
	// images
	CharacterSprites [][][]*ebiten.Image
	MagicArrows      [][]*ebiten.Image
	TileSprites      []*ebiten.Image
	CoinSprites      []*ebiten.Image
	PotionSprites    []*ebiten.Image
	TorchSprites     []*ebiten.Image
	ArrowSprite      *ebiten.Image
	BowSprite        *ebiten.Image
	BowSpriteDrawn   *ebiten.Image
	SwordSprite      *ebiten.Image
	StuffSprite      *ebiten.Image

	// audio
	ArrowSound                []byte
	ArrowHitSound             []byte
	SwordHitSound             []byte
	MagicArrowSound           []byte
	MWhatCanIDoForYouSound    []byte
	MStayAwhileAndListerSound []byte
	FOkey                     []byte
	FOhMyGoodnessGracious     []byte
}

func loadAudioFile(name string) ([]byte, error) {
	file, err := os.Open(fmt.Sprintf("assets/audio/%s", name))
	if err != nil {
		return nil, err
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		return nil, err
	}
	bs := make([]byte, stat.Size())
	_, err = bufio.NewReader(file).Read(bs)
	if err != nil {
		return nil, err
	}

	var o io.Reader
	ext := path.Ext(file.Name())
	switch ext {
	case ".mp3":
		o, err = mp3.DecodeWithoutResampling(bytes.NewReader(bs))
		if err != nil {
			return nil, err
		}
	case ".ogg":
		o, err = vorbis.DecodeWithoutResampling(bytes.NewReader(bs))
		if err != nil {
			return nil, err
		}
	case ".wav":
		o, err = wav.DecodeWithoutResampling(bytes.NewReader(bs))
		if err != nil {
			return nil, err
		}
	}

	b, err := io.ReadAll(o)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func LoadAssets() (*Assets, error) {
	a := &Assets{}

	// loading all sprites
	// character sprites
	characterModels := []string{
		"angel", "big_demon", "big_zombie",
		"doc", "dwarf_f", "dwarf_m",
		"elf_f", "elf_m", "goblin",
		"ice_zombie", "imp", "knight_f",
		"knight_m", "lizard_f", "lizard_m",
		"masked_orc", "muddy", "necromancer",
		"ogre", "orc_shaman", "orc_warrior",
		"pumpkin_dude", "skeleton", "slug",
		"swampy", "tiny_slug", "tiny_zombie",
		"wizzard_f", "wizzard_m", "wogol", "zombie",
	}
	var err error
	a.CharacterSprites, err = loadCharactersSprites(characterModels)
	if err != nil {
		return nil, err
	}

	// weapon
	a.BowSprite, _, err = ebitenutil.NewImageFromFile("assets/images/weapons/bow.png")
	if err != nil {
		return nil, err
	}
	a.BowSpriteDrawn, _, err = ebitenutil.NewImageFromFile("assets/images/weapons/bow_loaded.png")
	if err != nil {
		return nil, err
	}
	a.SwordSprite, _, err = ebitenutil.NewImageFromFile("assets/images/weapons/sword.png")
	if err != nil {
		return nil, err
	}

	a.StuffSprite, _, err = ebitenutil.NewImageFromFile("assets/images/weapons/stuff.png")
	if err != nil {
		return nil, err
	}

	// arrows
	a.ArrowSprite, _, err = ebitenutil.NewImageFromFile("assets/images/weapons/arrow.png")
	if err != nil {
		return nil, err
	}

	// magic
	a.MagicArrows = make([][]*ebiten.Image, 3)
	for i := 0; i < 3; i++ {
		temp := make([]*ebiten.Image, 30)
		for j := 0; j < 30; j++ {
			s, _, err := ebitenutil.NewImageFromFile(
				fmt.Sprintf("assets/images/magic_arrow/%d/1_%d.png", i+1, j),
			)
			if err != nil {
				return nil, err
			}
			temp[j] = s.SubImage(image.Rect(30, 40, 75, 60)).(*ebiten.Image)
		}
		a.MagicArrows[i] = temp
	}

	// torch
	a.TorchSprites = make([]*ebiten.Image, 8)
	for i := 0; i < 8; i++ {
		s, _, err := ebitenutil.NewImageFromFile(
			fmt.Sprintf("assets/images/torch/%d.png", i),
		)
		if err != nil {
			return nil, err
		}
		a.TorchSprites[i] = s
	}

	// items
	// coin
	a.CoinSprites = make([]*ebiten.Image, 4)
	for i := 0; i < 4; i++ {
		s, _, err := ebitenutil.NewImageFromFile(
			fmt.Sprintf("assets/images/items/coin_f%d.png", i),
		)
		if err != nil {
			return nil, err
		}
		a.CoinSprites[i] = s
	}

	// potions redPotionSprite
	potionsTypes := []string{"red", "green", "yellow", "blue"}
	a.PotionSprites = make([]*ebiten.Image, len(potionsTypes))
	for i, t := range potionsTypes {
		s, _, err := ebitenutil.NewImageFromFile(
			fmt.Sprintf("assets/images/items/potion_%s.png", t),
		)
		if err != nil {
			return nil, err
		}
		a.PotionSprites[i] = s
	}

	// tile map
	a.TileSprites = make([]*ebiten.Image, TileTypes)
	for i := 0; i < TileTypes; i++ {
		s, _, err := ebitenutil.NewImageFromFile(
			fmt.Sprintf("assets/images/tiles/%d.png", i),
		)
		if err != nil {
			return nil, err
		}
		a.TileSprites[i] = s
	}
	// end loading all sprites

	// audio
	a.ArrowSound, err = loadAudioFile("arrow.mp3")
	if err != nil {
		return nil, err
	}
	a.ArrowHitSound, err = loadAudioFile("arrow_hit.mp3")
	if err != nil {
		return nil, err
	}
	a.SwordHitSound, err = loadAudioFile("sword_hit.mp3")
	if err != nil {
		return nil, err
	}
	a.MagicArrowSound, err = loadAudioFile("magic_arrow.mp3")
	if err != nil {
		return nil, err
	}
	a.MWhatCanIDoForYouSound, err = loadAudioFile("what_can_i_do_for_you.mp3")
	if err != nil {
		return nil, err
	}
	a.MStayAwhileAndListerSound, err = loadAudioFile("stay-awhile-and-lister.ogg")
	if err != nil {
		return nil, err
	}
	a.FOkey, err = loadAudioFile("woman_okey.mp3")
	if err != nil {
		return nil, err
	}
	a.FOhMyGoodnessGracious, err = loadAudioFile("oh-my-goodness-gracious.wav")
	if err != nil {
		return nil, err
	}

	return a, nil
}
