package game

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Bow struct {
	name string
	*Rect
	*DamageStats
	angle float64

	sprite      *ebiten.Image
	spriteDrawn *ebiten.Image
	isDrawn     bool
	arrowSprite *ebiten.Image
	op          *ebiten.DrawImageOptions

	arrowSound    []byte
	arrowHitSound []byte

	attackTimer    float64
	attackCooldown float64
}

func NewBow(
	name string,
	a *Assets,
	attackCooldown float64,
	ds *DamageStats,
) *Bow {
	w := &Bow{
		name:  name,
		Rect:  &Rect{},
		angle: 0,

		sprite:      a.BowSprite,
		spriteDrawn: a.BowSpriteDrawn,
		isDrawn:     false,
		arrowSprite: a.ArrowSprite,
		op:          &ebiten.DrawImageOptions{},

		// damage
		DamageStats: ds,

		attackTimer:    0,
		attackCooldown: attackCooldown,

		arrowSound:    a.ArrowSound,
		arrowHitSound: a.ArrowHitSound,
	}

	w.width = float64(w.sprite.Bounds().Dx())
	w.height = float64(w.sprite.Bounds().Dy())

	w.attackTimer = w.attackCooldown * FPS

	return w
}

func (w *Bow) Update(cameraX, cameraY float64, g *Game) {

	// player center
	w.x, w.y = g.player.CenterXY()

	// weapon center
	centerX, centerY := w.CenterXY()
	centerX += cameraX
	centerY += cameraY

	// calc angle for weapon rotation
	cx, cy := ebiten.CursorPosition()
	w.angle = math.Atan2(float64(cy)-centerY, float64(cx)-centerX)

	// fire arrow
	w.attackTimer++
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) &&
		w.attackTimer >= w.attackCooldown*FPS &&
		!w.isDrawn {
		w.isDrawn = true
	}
	if inpututil.IsMouseButtonJustReleased(ebiten.MouseButton0) && w.isDrawn {
		g.audioPlayer.PlaySound(w.arrowSound)
		arrow := NewBowArrow(
			w.arrowSprite,
			w.arrowHitSound,
			w.x,
			w.y,
			w.angle,
			ArrowScale,
			math.Pi/2,
			10,
			w.DamageStats,
		)
		w.attackTimer = 0
		w.isDrawn = false
		g.arrows = append(g.arrows, arrow)
	}
}

func (w *Bow) Draw(screen *ebiten.Image, p *Player) {
	w.op.GeoM.Reset()

	// scale and rotate
	w.op.GeoM.Translate(5, -13)
	w.op.GeoM.Scale(BowScale, BowScale)
	w.op.GeoM.Rotate(w.angle)
	w.op.GeoM.Scale(1, 1)

	w.op.GeoM.Translate(p.CenterXY())

	if w.isDrawn {
		screen.DrawImage(w.spriteDrawn, w.op)
	} else {
		screen.DrawImage(w.sprite, w.op)
	}
}

func (b *Bow) GetIcon() *ebiten.Image {
	return b.sprite
}

func (b *Bow) GetName() string {
	return b.name
}
