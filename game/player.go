package game

import (
	"image/color"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

type Player struct {
	name string
	*CharacterAnimator
	*Mover
	*CharacterStats

	isVisible bool

	inventory *Inventory

	weapons      []Weapon
	activeWeapon int

	lineOfSight float64

	justUsedPotion bool
	potionTimer    float64
	potionCooldown float64
}

func NewPlayer(x, y float64, stats *CharacterStats, sprites [][]*ebiten.Image) *Player {

	p := &Player{
		name:              "John",
		CharacterAnimator: NewCharacterAnimator(x, y, sprites),
		CharacterStats:    stats,
		lineOfSight:       300,

		// inventory
		inventory: NewInventory(),

		// weapons
		activeWeapon: 0,
		weapons:      make([]Weapon, 0),

		// potions
		justUsedPotion: false,
		potionCooldown: 1,
		potionTimer:    0,
	}

	p.isVisible = true

	p.height = p.width

	return p
}

func (p *Player) AddWeapon(w ...Weapon) {
	p.weapons = append(p.weapons, w...)
}

func (p *Player) Update(g *Game) (float64, float64) {
	if !p.IsAlive() {
		// TODO change state
		log.Println("You dead!")
		return 0, 0
	}

	// handle weapons
	// 44 - Key1, 45 - Key2 and so on
	p.HandleWeaponChange()

	p.CheckPotionCooldown()

	cx, cy := p.handleMovement(g.currentLevel.obstacleTiles)
	p.CharacterAnimator.Update(0, 0, g.currentLevel)

	p.weapons[p.activeWeapon].Update(cx, cy, g)
	return cx, cy
}

func (p *Player) HandleWeaponChange() {
	// change weapons with keyboard buttons
	for i := 0; i < len(p.weapons); i++ {
		if inpututil.IsKeyJustPressed(ebiten.Key(44 + i)) {
			p.activeWeapon = i
		}
	}

	// change weapons with wheel
	_, dy := ebiten.Wheel()
	if dy != 0 {
		p.activeWeapon++
		if p.activeWeapon >= len(p.weapons) {
			p.activeWeapon = 0
		}
	}
}

// move to separate func to call in inventory state without updating whole player
func (p *Player) CheckPotionCooldown() {
	if p.justUsedPotion {
		p.potionTimer++
		if (p.potionTimer) >= p.potionCooldown*FPS {
			p.justUsedPotion = false
			p.potionTimer = 0
		}
	}
}

func (p *Player) Draw(screen *ebiten.Image, g *Game) {
	p.CharacterAnimator.Draw(screen, GAP)

	// debug
	if g.debug.Player {
		vector.DrawFilledRect(
			screen, float32(p.x),
			float32(p.y),
			float32(p.width),
			float32(p.height),
			color.RGBA{0, 0, 255, 0},
			false)
	}

	//weapon
	p.weapons[p.activeWeapon].Draw(screen, p)
}

// player handleMovement
func (p *Player) handleMovement(obstacles []*Tile) (float64, float64) {
	dx := 0.0
	dy := 0.0

	if ebiten.IsKeyPressed(ebiten.KeyW) {
		dy -= p.speed
	}
	if ebiten.IsKeyPressed(ebiten.KeyS) {
		dy += p.speed
	}
	if ebiten.IsKeyPressed(ebiten.KeyA) {
		dx -= p.speed
	}
	if ebiten.IsKeyPressed(ebiten.KeyD) {
		dx += p.speed
	}

	p.Move(dx, dy, p.CharacterAnimator, obstacles)

	var cameraX, cameraY float64
	if (p.x + p.width) > ScreenWidth-CameraTreshholdX {
		cameraX = ScreenWidth - CameraTreshholdX - (p.x + p.width)
		p.x = ScreenWidth - CameraTreshholdX - p.width
	}
	if p.x < CameraTreshholdX {
		cameraX = CameraTreshholdX - p.x
		p.x = CameraTreshholdX
	}
	if (p.y + p.height) > ScreenHeight-CameraTreshholdY {
		cameraY = ScreenHeight - CameraTreshholdY - (p.y + p.height)
		p.y = ScreenHeight - CameraTreshholdY - p.height
	}
	if p.y < CameraTreshholdY {
		cameraY = CameraTreshholdY - p.y
		p.y = CameraTreshholdY
	}

	return cameraX, cameraY
}
