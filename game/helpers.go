package game

import (
	"fmt"
	"image/color"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"github.com/tinne26/etxt"
)

func Distance(sx, sy, tx, ty float64) float64 {
	return math.Sqrt(math.Pow(sx-tx, 2) + math.Pow(sy-ty, 2))
}

// return inventory slot and index
func GetInventorySlot(
	cx, cy float64,
	invStartX, invStartY float64,
	inventory *Inventory,
) (Usable, int) {
	x := math.Floor((cx-invStartX)/InventoryIconSize) - 1
	if x >= MaxSlotsInRow {
		x = MaxSlotsInRow
	}
	y := math.Floor((cy - invStartY) / InventoryIconSize)
	y *= MaxSlotsInRow
	idx := int(x + y)

	if cx < invStartX || cx > invStartX+((MaxSlotsInRow+1)*InventoryIconSize) ||
		cy < invStartY || cy > invStartY+((y+1)*InventoryIconSize) {
		return nil, 0
	}

	if idx < 0 || idx >= len(inventory.slots) {
		return nil, 0
	}

	return inventory.slots[idx], idx
}

func DrawCharacterInfo(
	tx *etxt.Renderer,
	op *ebiten.DrawImageOptions,
	targetImg, charImage *ebiten.Image,
	name string,
	level int,
	stats *CharacterStats,
	inv *Inventory,
) {
	tx.SetAlign(etxt.Top, etxt.Left)

	// player image
	op.GeoM.Reset()
	op.GeoM.Scale(5, 5)
	op.GeoM.Translate(20, 0)
	op.GeoM.Scale(1, 1)
	targetImg.DrawImage(charImage, op)

	// player name
	tx.SetSizePx(20)
	tx.Draw(name, 150, 20)

	// level
	tx.SetSizePx(15)
	tx.Draw(fmt.Sprintf("Level: %d", level), 150, 50)

	tx.Draw(fmt.Sprintf("Gold: %d", inv.gold), 230, 50)

	// HP
	tx.Draw(fmt.Sprintf("HP: %d / %d", stats.health, stats.maxHealth), 150, 70)

	// MP
	tx.Draw(fmt.Sprintf("MP: %d / %d", stats.mana, stats.maxMana), 150, 90)
}

func DrawInventoryHeader(
	tr *etxt.Renderer,
	op *ebiten.DrawImageOptions,
	targetImg, screen *ebiten.Image,
	rect Rect,
	inv *Inventory,
) {
	// inventory line
	vector.StrokeLine(targetImg, 20, 175, float32(rect.width-20), 175, 3, color.Black, true)
	// inventory title
	tr.SetAlign(etxt.Bottom, etxt.XCenter)
	tr.Draw(
		fmt.Sprintf("Inventory space %d / %d",
			inv.Len(),
			inv.maxSize,
		), int(rect.width/2), 195)
	// inventory window
	op.GeoM.Reset()
	op.GeoM.Translate(rect.XY())
	screen.DrawImage(targetImg, op)
}

func DrawInventory(
	op *ebiten.DrawImageOptions,
	screen *ebiten.Image,
	inv *Inventory,
	invStartX, invStartY float64,
) {
	xPos := invStartX
	yPos := invStartY
	counter := 0
	for idx, s := range inv.slots {
		if idx != 0 && counter%MaxSlotsInRow == 0 {
			yPos += InventoryIconSize
			xPos = invStartX
			counter = 0
		}

		xPos += InventoryIconSize

		counter++

		op.GeoM.Reset()
		op.GeoM.Scale(InventoryIconScale, InventoryIconScale)
		op.GeoM.Scale(1, 1)
		op.GeoM.Translate(xPos, yPos)

		screen.DrawImage(s.Icon(), op)
	}
}

func DrawGameInfo(screen *ebiten.Image, g *Game) {
	vector.DrawFilledRect(screen, 0, 0, ScreenWidth, 30, UIColor, false)

	// HP
	g.textRenderer.SetColor(color.Black)
	g.textRenderer.SetTarget(screen)
	g.textRenderer.SetAlign(etxt.YCenter, etxt.XCenter)
	g.textRenderer.SetSizePx(20)
	// HP
	g.textRenderer.Draw("HP", 20, 15)

	vector.DrawFilledRect(
		screen,
		40,
		12,
		100,
		5,
		color.RGBA{220, 220, 220, 255},
		false,
	)
	hpInPercent := (100 * g.player.health) / g.player.maxHealth
	vector.DrawFilledRect(
		screen,
		40,
		12,
		float32(hpInPercent),
		5,
		color.RGBA{255, 0, 0, 255},
		false,
	)
	// MP
	g.textRenderer.Draw("MP", 200, 15)
	vector.DrawFilledRect(
		screen,
		220,
		12,
		100,
		5,
		color.RGBA{220, 220, 220, 255},
		false,
	)
	mpInPercent := (100 * g.player.mana) / g.player.maxMana
	vector.DrawFilledRect(
		screen,
		220,
		12,
		float32(mpInPercent),
		5,
		color.RGBA{24, 120, 222, 255},
		false,
	)

	// coins
	g.textRenderer.Draw(fmt.Sprintf("Gold: %d", g.player.inventory.gold), ScreenWidth-80, 15)
}

func DDA(start, target *Rect, l *Level, maxRange float64) bool {
	// start pos
	startX, startY := start.CenterXY()
	targetX, targetY := target.CenterXY()

	// direction
	dirX := targetX - startX
	dirY := targetY - startY
	mag := 1 / math.Sqrt(float64(dirX*dirX+dirY*dirY))

	// normalized direction
	dirXN := float64(dirX) * mag
	dirYN := float64(dirY) * mag

	vRayUnitStepSizeX := math.Sqrt(1 + (dirYN/dirXN)*(dirYN/dirXN))
	vRayUnitStepSizeY := math.Sqrt(1 + (dirXN/dirYN)*(dirXN/dirYN))

	vMapCheckX := int(startX)
	vMapCheckY := int(startY)

	var vRayLen1DX, vRayLen1DY float64
	var vStepX, vStepY int

	if dirXN < 0 {
		vStepX = -1
		vRayLen1DX = (startX - float64(vMapCheckX)) * vRayUnitStepSizeX
	} else {
		vStepX = 1
		vRayLen1DX = (float64(vMapCheckX+1) - startX) * vRayUnitStepSizeX
	}
	if dirYN < 0 {
		vStepY = -1
		vRayLen1DY = (startY - float64(vMapCheckY)) * vRayUnitStepSizeY
	} else {
		vStepY = 1
		vRayLen1DY = (float64(vMapCheckY+1) - startY) * vRayUnitStepSizeY
	}

	tileFound := false
	distanceX, distanceY := 0.0, 0.0
	for !tileFound {
		if distanceX > maxRange && distanceY > maxRange {
			return false
		}
		// walk
		if vRayLen1DX < vRayLen1DY {
			vMapCheckX += vStepX
			distanceX = vRayLen1DX
			vRayLen1DX += vRayUnitStepSizeX
		} else {
			vMapCheckY += vStepY
			distanceY = vRayLen1DY
			vRayLen1DY += vRayUnitStepSizeY
		}

		if vMapCheckX >= 0 && vMapCheckX < ScreenWidth &&
			vMapCheckY >= 0 && vMapCheckY < ScreenHeight {
			t := l.GetTile(vMapCheckX, vMapCheckY)

			if t != nil && t.isObstacle {
				tileFound = true
			}

			if vMapCheckX == int(targetX) && vMapCheckY == int(targetY) {
				return true
			}

		}
	}

	return !tileFound
}
