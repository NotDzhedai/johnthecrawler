package game

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type Potion interface {
	Usable
	Item
}

// Base potion
type potionExecFunc func(p *Player)

type basePotion struct {
	*baseItem
	name      string
	isVisited bool
	execFun   potionExecFunc
}

func newBasePotion(name string, x, y float64, sprite *ebiten.Image, execFun potionExecFunc) *basePotion {
	i := newBaseItem(x, y, false, []*ebiten.Image{sprite}, 10)
	p := &basePotion{
		name:      name,
		baseItem:  i,
		isVisited: false,
		execFun:   execFun,
	}

	return p
}

func (pot *basePotion) Name() string {
	return pot.name
}

func (pot *basePotion) Icon() *ebiten.Image {
	return pot.sprites[0]
}

func (pot *basePotion) Price() int {
	return pot.price
}

func (pot *basePotion) Use(p *Player) bool {
	if p.justUsedPotion {
		return false
	}

	pot.execFun(p)

	pot.toDelete = true
	return true
}

func (pot *basePotion) Update(cameraX, cameraY float64, l *Level, p *Player) {
	if pot.IsCollide(p.Rect) {
		if p.inventory.Add(pot) {
			pot.toDelete = true
		}
	}

	pot.baseItem.Update(cameraX, cameraY, l, p)
}

// Health potion
type RedPotion struct {
	*basePotion
}

func NewRedPotion(x, y float64, value int, sprite *ebiten.Image) *RedPotion {
	rd := &RedPotion{}

	rd.basePotion = newBasePotion("Health potion", x, y, sprite, func(p *Player) {
		p.health += value
		if p.health > p.maxHealth {
			p.health = p.maxHealth
		}
		p.justUsedPotion = true
	})

	return rd
}

// Mana potion
type BluePotion struct {
	*basePotion
}

func NewBluePotion(x, y float64, value int, sprite *ebiten.Image) *RedPotion {
	rd := &RedPotion{}

	rd.basePotion = newBasePotion("Mana potion", x, y, sprite, func(p *Player) {
		p.mana += value
		if p.mana > p.maxMana {
			p.mana = p.maxMana
		}
		p.justUsedPotion = true
	})

	return rd
}
