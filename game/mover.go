package game

import (
	"math"
)

type Mover struct {
}

func (m *Mover) Move(dx, dy float64, p *CharacterAnimator, obstacles []*Tile) {
	p.isRunning = false

	if dx != 0 || dy != 0 {
		p.isRunning = true
	}

	// diagonal movement
	if dx != 0 && dy != 0 {
		dx = dx * (math.Sqrt(2) / 2)
		dy = dy * (math.Sqrt(2) / 2)
	}

	if dx < 0 {
		p.flip = true
	} else if dx > 0 {
		p.flip = false
	}

	p.x += dx
	// horizotal collision
	for _, obs := range obstacles {
		if p.IsCollide(obs.Rect) {
			if dx > 0 {
				p.x = obs.x - p.width
				p.isRunning = false
			}
			if dx < 0 {
				p.x = obs.x + obs.width
				p.isRunning = false
			}
		}
	}

	p.y += dy
	// vertical collision
	for _, obs := range obstacles {
		if p.IsCollide(obs.Rect) {
			if dy > 0 {
				p.y = obs.y - p.height
				p.isRunning = false

			}
			if dy < 0 {
				p.y = obs.y + obs.height
				p.isRunning = false
			}
		}
	}
}
