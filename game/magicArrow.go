package game

import (
	"github.com/hajimehoshi/ebiten/v2"
)

type MagicArrow struct {
	*BaseArrow
	attackTimer    float64
	attackCooldown float64

	lifeTimer    float64
	lifeCooldown float64
}

func NewMagicArrow(
	images []*ebiten.Image,
	sound []byte,
	x, y, angle, scale, rotation, speed float64,
	ds *DamageStats,
	attackCooldown float64,
) *MagicArrow {
	ma := &MagicArrow{
		BaseArrow:      NewBaseArrow(images, sound, x, y, angle, scale, rotation, speed, ds, true),
		attackTimer:    0,
		attackCooldown: attackCooldown,
		lifeCooldown:   2,
	}

	ma.isVisible = true
	ma.offsetX = 0
	ma.offsetY = -(ma.height / 2)

	ma.attackTimer = ma.attackCooldown * FPS

	return ma
}

func (ma *MagicArrow) Update(cameraX, cameraY float64, l *Level, audioPlayer *AudioPlayer) *CombatText {
	ma.BaseArrow.Update(cameraX, cameraY)

	ma.attackTimer++
	ma.lifeTimer++
	for _, enemy := range l.enemies {
		if ma.attackTimer >= ma.attackCooldown*FPS && ma.IsCollide(enemy.Rect) && enemy.isAlive {
			ct := ma.MakeDamage(enemy)
			ma.attackTimer = 0
			return ct
		}
	}

	if ma.lifeTimer >= ma.lifeCooldown*FPS {
		ma.canDelete = true
	}

	return nil
}
