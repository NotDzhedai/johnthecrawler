package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/NotDzhedai/JohnTheCrawler/game"
)

func main() {
	g, err := game.NewGame()
	if err != nil {
		panic(err)
	}

	ebiten.SetWindowSize(game.ScreenWidth, game.ScreenHeight)
	ebiten.SetWindowTitle("John the Crawler")

	if err := ebiten.RunGame(g); err != nil {
		panic(err)
	}
}
