# John the Crawler

## Dungeon crawling game written in [ebiten](https://ebitengine.org/)

## Todo

- Line of sight for the player ✅
- Inventory ✅
- Dialogs ✅
- Vendors ✅
- Multiple levels ✅
- Chests ❌
- Behavior of enemies ❌
- Doors and keys ❌
- Random level generation ❌

## Screenshots

![](images/1.png)
![](images/2.png)
![](images/3.png)
